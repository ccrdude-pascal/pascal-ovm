{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FireflyOVM;

{$warn 5023 off : no warning about unused units}
interface

uses
  OVM.Windows.Consts, OVM.Basics, OVM.Attributes, OVM.ComboBox, 
  OVM.ComboBoxEx, OVM.ComboBox.Attributes, OVM.ListView, 
  OVM.ListView.Attributes, OVM.ListView.JSON, OVM.TreeView, 
  OVM.TreeView.Attributes, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('FireflyOVM', @Register);
end.
