﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Listview enhancement offering XP Style (common controls 6.0) grouping.)

   @see(http://www.delphipraxis.net/topic3568_tilemodus+und+gruppierung+von+listviewitems+winxp.html)
   @see(https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features#groupsubsets)

   @preformatted(
// *****************************************************************************
// Copyright: © 2007-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-18  pk   10m  Added TDate support
// 2021-12-05  pk   10m  Removed TXPListView
// 2021-12-03  pk   10m  Aded TListViewGroupWrapper to simplify group handling.
// 2017-02-22  pk    5m  [CCD] Header update
// 2017-02-22  pk    5m  [CCD] Renamed from snlUIListView
// 2007-07-02  pk   10m  Integrated CommCtrl_Fragment
// 2007-06-29  pk   30m  Created class helpers for modern Delphi
// *****************************************************************************
// * This will NOT work when there is no manifest included in the executable!
// * On Windows older than XP, new functions will not work
// * Credits go to Matthias Simmack and his posts @ Delphi-PRAXiS:
// *****************************************************************************
   )
}

unit OVM.ListView;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$modeswitch prefixedattributes}
//{$modeswitch multiscopehelpers}
{$ENDIF FPC}

interface

uses
   SysUtils,
   {$IFDEF MSWindows}
   Windows,
   CommCtrl,
   {$ENDIF MSWindows}
   Classes,
   Controls,
   TypInfo,
   Graphics, // for TColor in interface
   ComCtrls,
   Generics.Collections,
   DebugLog,
   Rtti,
   OVM.Basics,
   {$IFDEF MSWindows}
   OVM.Windows.Consts,
   {$ENDIF MSWindows}
   OVM.Attributes,
   OVM.ListView.Attributes,
   OVM.TreeView.Attributes;

type

   /// TViewStyleEx is the extended version of TViewStyle, including vsTile available since Windows XP
   TViewStyleEx = (vsIcon, vsSmallIcon, vsList, vsReport, vsTile);

   { TLVGroupHelper }

   {$IFDEF MSWindows}
   /// This helper allows to initialize a TLVGroup with empty content, and a specific GroupID and mask.
   TLVGroupHelper = record helper for TLVGroup
      procedure Init(AGroupID: integer; AMask: UINT);
   end;

   {$ENDIF MSWindows}

   /// TListViewGroupAlign is intended to align group headers and footers.
   TListViewGroupAlign = (lvgaFooterCenter, lvgaFooterLeft, lvgaFooterRight, lvgaHeaderCenter, lvgaHeaderLeft, lvgaHeaderRight);

   { TListViewGroupWrapper }

   TListViewGroupWrapper = class
   private
      FGroupID: integer;
      FListView: TListView;
      {$IFDEF MSWindows}
      function GetGroup(AMask: UINT; out AGroup: TLVGroup): boolean;
      function SetGroupState(AState: UINT; ASet: boolean): boolean;
      function GetGroupState(AState: UINT; ADefault: boolean): boolean;
      {$ENDIF MSWindows}
   private
      function GetCollapsed: boolean;
      function GetCollapsible: boolean;
      function GetDescriptionBottom: WideString;
      function GetDescriptionTop: WideString;
      function GetExtendedImage: integer;
      function GetFooter: WideString;
      function GetFooterAlignment: TAlignment;
      function GetHeader: WideString;
      function GetHeaderAlignment: TAlignment;
      function GetSubSeted: boolean;
      function GetSubSetTitle: WideString;
      function GetSubTitle: WideString;
      function GetTitleImage: integer;
      procedure SetCollapsed(AValue: boolean);
      procedure SetCollapsible(AValue: boolean);
      procedure SetDescriptionBottom(AValue: WideString);
      procedure SetDescriptionTop(AValue: WideString);
      procedure SetExtendedImage(AValue: integer);
      procedure SetFooter(AValue: WideString);
      procedure SetFooterAlignment(AValue: TAlignment);
      procedure SetHeader(AValue: WideString);
      procedure SetHeaderAlignment(AValue: TAlignment);
      procedure SetSubItemCount(AValue: integer);
      procedure SetSubSeted(AValue: boolean);
      procedure SetSubSetTitle(AValue: WideString);
      procedure SetSubTitle(AValue: WideString);
      procedure SetTitleImage(AValue: integer);
   public
      constructor Create(AListView: TListView; AGroupID: integer);
      procedure SetSubset(ACount: integer; AMore: WideString);
      property GroupID: integer read FGroupID;
      property Collapsed: boolean read GetCollapsed write SetCollapsed;
      property Collapsible: boolean read GetCollapsible write SetCollapsible;
      property SubSeted: boolean read GetSubSeted write SetSubSeted;
      property DescriptionBottom: WideString read GetDescriptionBottom write SetDescriptionBottom;
      property DescriptionTop: WideString read GetDescriptionTop write SetDescriptionTop;
      property ExtendedImage: integer read GetExtendedImage write SetExtendedImage;
      property Footer: WideString read GetFooter write SetFooter;
      property FooterAlignment: TAlignment read GetFooterAlignment write SetFooterAlignment;
      property Header: WideString read GetHeader write SetHeader;
      property HeaderAlignment: TAlignment read GetHeaderAlignment write SetHeaderAlignment;
      property SubTitle: WideString read GetSubTitle write SetSubTitle;
      property SubSetTitle: WideString read GetSubSetTitle write SetSubSetTitle;
      property TitleImage: integer read GetTitleImage write SetTitleImage;
      property SubItemCount: integer write SetSubItemCount;
   end;

   { TListViewGroupingHelper }

   TListViewGroupingHelper = class helper for TListView
   private
      function GetGroupView: boolean;
      procedure SetGroupImages(AValue: TImageList);
      procedure SetGroupView(const AValue: boolean);
      function GetViewStyleEx: TViewStyleEx;
      procedure SetViewStyleEx(const AValue: TViewStyleEx);
      procedure SetTileViewLines(const AValue: integer);
      function GetHeaderInAllViews: boolean;
      procedure SetHeaderInAllViews(const AValue: boolean);
   public
      procedure AddColumn(const Header: WideString; AWidth: integer; AAlign: TAlignment = taLeftJustify; AAutoSize: boolean = False);
      procedure AddGroup(const AHeader: WideString; const AGroupId, AImageIndex: integer);
      procedure UpdateGroup(const AHeader: WideString; const AGroupId: integer);
      function GetGroupName(const GroupId: integer): WideString;
      {$IFDEF FPC}
      function Group(AID: integer): TListViewGroupWrapper;
      /// GroupView enables grouping; if set, each TListItem needs a GroupId assigned to be visible.
      property GroupView: boolean read GetGroupView write SetGroupView;
      property GroupImages: TImageList write SetGroupImages;
      {$ENDIF FPC}
      property HeaderInAllViews: boolean read GetHeaderInAllViews write SetHeaderInAllViews;
      /// The ViewStyleEx property supports the "new" (starting XP) mode vsTile
      property ViewStyleEx: TViewStyleEx read GetViewStyleEx write SetViewStyleEx;
      /// The TileViewLines property sets how many subitems are shown in mode vsTile
      property TileViewLines: integer write SetTileViewLines;
   end;

   { TListItemGroupingHelper }

   TListItemGroupingHelper = class helper for TListItem
   private
      function GetGroupID: integer;
      procedure SetGroupID(const Value: integer);
   public
      procedure SetMinimumSubItemCount(const ACount: integer; const AEmptyFiller: string = '');
      procedure SetTileViewColumns(const AColumnCount: integer; const AColumns: array of integer);
      {$IFDEF FPC}
      property GroupId: integer read GetGroupID write SetGroupID;
      {$ELSE FPC}
      {$IFDEF Ver180}
      property GroupId: integer read GetGroupID write SetGroupID;
      {$ENDIF Ver180}
   {$ENDIF FPC}
   end;

   { TObjectListViewClassHelper }

   TObjectListViewClassHelper = class helper(TListViewGroupingHelper) for TListView
   private
      procedure ObjectDblClick({%H-}Sender: TObject);
   public
      function HasColumn(AName: string): boolean;
      function FindOrCreateColumn(AName: string): TListColumn;
      function AddColumn(AName: string): TListColumn;
      procedure UpdateColumnsForClass(AnItem: TObject);
      function HasPublishedProperties(AnItem: TObject): boolean;
      function UpdateListItemForObject(AListItem: TListItem; AnItem: TObject; AGroups: TStrings = nil; AGroupProperty: string = ''): integer;
      {
         AddToPropertyList adds a single items properties to a list,
         designed to have separate Group IDs per object.
         You need to manually create the group first.
      }
      function AddToPropertyList(AnItem: TObject; AGroupID: integer): integer;
      {
         DisplayEnumerable<T> allows to add all items of a generic collection
         to a listview.
      }
      procedure DisplayItems<T: class>(TheItems: TEnumerable<T>); overload;
      {
         DisplayItems wants to display all items, but so far only adds
         TCollection to a listview.
      }
      procedure DisplayItems(AList: TCollection); overload;
      procedure DisplayItems<T: class>(AnEnumerator: TEnumerator<T>); overload;
      {
         DisplayProperties adds an item to a listview used as a property
         list.
      }
      procedure DisplayProperties(AnItem: TObject);
      function GetSelectedItem<T: class>: T;
      procedure SetSelectedItem<T: class>(AnItem: T);
   end;

procedure FillListViewFromObjectList<T>(AListView: TListView; TheItems: TEnumerable<T>);

{$IFDEF MSWindows}
function ListViewGetGroupViewEnabled(const AListView: TListView): boolean; inline;
procedure ListViewSetGroupViewEnabled(const AListView: TListView; AEnabled: boolean); inline;
procedure ListViewAddGroup(const AListView: TListView; const AHeader: WideString; const AGroupId: integer; AFooter: WideString = '';
   AImageIndex: integer = -1);
procedure ListViewRenameGroup(const AListView: TListView; const AHeader: WideString; const AGroupId: integer; AFooter: WideString = '');
function ListViewGetGroupName(const AListView: TListView; const AGroupId: integer; var AHeader: WideString): boolean;
procedure ListViewSetTileInfo(const AListView: TListView; const ALineCount: integer); overload;
procedure ListViewSetTileInfo(const AListView: TListView; const ALineCount, ACX, ACY: integer); overload;

function ListItemGetGroupId(const AListItem: TListItem): integer; inline;
procedure ListItemSetGroupId(const AListItem: TListItem; const AGroupId: integer); // inline;
procedure ListItemSetMinimumSubItemCount(const AListItem: TListItem; const ACount: integer; const AEmptyFiller: string = ''); inline;
procedure ListItemTileViewColumns(const AListItem: TListItem; const AColumnCount: integer; const AColumns: array of integer);
{$ENDIF MSWindows}

/// Tests whether this OS supports ListView Groups
function WindowsSupportsListViewGroups: boolean;

implementation

var
   GIsWindowsXPOrLater: boolean;

procedure FillListViewFromObjectList<T>(AListView: TListView; TheItems: TEnumerable<T>);
var
   item: T;
   li: TListItem;
   b: boolean;
begin
   AListView.Items.BeginUpdate;
   try
      AListView.Items.Clear;
      b := False;
      for item in TheItems do begin
         if not b then begin
            AListView.UpdateColumnsForClass(item);
            b := True;
         end;
         li := AListView.Items.Add;
         AListView.UpdateListItemForObject(li, item);
      end;
   finally
      AListView.Items.EndUpdate;
   end;
end;

(*
 * Tests the OS version
 *
 * @return(True if Windows version is XP or later)
 *)
function MyWindowsIsXPOrLater: boolean; // normally part of PepiK.Windows
   {$IFDEF MSWindows}
var
   winVerInfo: TOSVersionInfoW;
   {$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   winVerInfo.dwOSVersionInfoSize := sizeof(winVerInfo);
   GetVersionExW(winVerInfo);
   Result := (winVerInfo.dwMajorVersion > 5) or ((winVerInfo.dwMajorVersion = 5) and (winVerInfo.dwMinorVersion > 0));
   if FindCmdLineSwitch('simulate-before-xp') then begin
      Result := False;
   end;
   {$ELSE MSWindows}
   Result := False;
   {$ENDIF MSWindows}
end;

{$IFDEF MSWindows}
function ListViewGetHeaderInAllViews(const ListView: TListView): boolean; inline;
begin
   Result := (ListView_GetExtendedListViewStyle(ListView.Handle) and LVS_EX_HEADERINALLVIEWS) > 0;
end;

procedure ListViewSetHeaderInAllViews(const ListView: TListView; Enabled: boolean); inline;
var
   c: cardinal;
begin
   c := ListView_GetExtendedListViewStyle(ListView.Handle);
   if Enabled then begin
      if (c and LVS_EX_HEADERINALLVIEWS) = 0 then begin
         c := c or LVS_EX_HEADERINALLVIEWS;
         ListView_SetExtendedListViewStyle(ListView.Handle, c);
      end;
   end else begin
      if (c and LVS_EX_HEADERINALLVIEWS) > 0 then begin
         c := c - LVS_EX_HEADERINALLVIEWS;
         ListView_SetExtendedListViewStyle(ListView.Handle, c);
      end;
   end;
end;

function ListViewGetGroupViewEnabled(const AListView: TListView): boolean; inline;
begin
   Result := ListView_IsGroupViewEnabled(AListView.Handle);
end;

procedure ListViewSetGroupViewEnabled(const AListView: TListView; AEnabled: boolean); inline;
begin
   ListView_EnableGroupView(AListView.Handle, AEnabled);
end;

procedure ListViewAddGroup(const AListView: TListView; const AHeader: WideString; const AGroupId: integer; AFooter: WideString; AImageIndex: integer);
var
   grp: CommCtrl.TLVGROUP;
begin
   if not Assigned(AListView) then begin
      Exit;
   end;
   if AListView.Handle = 0 then begin
      Exit;
   end;
   ZeroMemory(@grp, SizeOf(grp));
   grp.cbSize := SizeOf(grp);
   grp.mask := LVGF_HEADER or LVGF_GROUPID or LVGF_STATE or LVGF_TITLEIMAGE;
   grp.state := LVGS_COLLAPSIBLE;
   grp.iGroupId := AGroupID;
   grp.iTitleImage := AImageIndex;
   grp.pszHeader := pwidechar(AHeader);
   if Length(AFooter) > 0 then begin
      grp.pszFooter := pwidechar(AFooter);
      grp.mask := grp.mask or LVGF_FOOTER;
   end;
   CommCtrl.ListView_InsertGroup(AListView.Handle, AGroupID, WPARAM(@grp));
end;

procedure ListViewRenameGroup(const AListView: TListView; const AHeader: WideString; const AGroupId: integer; AFooter: WideString);
var
   G: TLVGroup;
begin
   if not Assigned(AListView) then begin
      Exit;
   end;
   if AListView.Handle = 0 then begin
      Exit;
   end;
   ZeroMemory(@G, SizeOf(G));
   G.cbSize := SizeOf(TLVGroup);
   G.mask := LVGF_HEADER;
   ListView_GetGroupInfo(AListView.Handle, AGroupId, G);
   G.pszHeader := pwidechar(AHeader);
   G.cchHeader := lstrlenW(G.pszHeader);
   if Length(AFooter) > 0 then begin
      G.pszFooter := pwidechar(AFooter);
      G.cchFooter := lstrlenW(G.pszFooter);
      G.mask := G.mask or LVGF_FOOTER;
   end;
   ListView_SetGroupInfo(AListView.Handle, AGroupId, G);
end;

function ListViewGetGroupName(const AListView: TListView; const AGroupId: integer; var AHeader: WideString): boolean;
var
   G: TLVGroup;
begin
   ZeroMemory(@G, SizeOf(G));
   G.cbSize := SizeOf(TLVGroup);
   G.mask := LVGF_HEADER or LVGF_GROUPID;
   G.pszHeader := nil;
   G.cchHeader := 0;
   G.iGroupId := AGroupId;
   Result := (ListView_GetGroupInfo(AListView.Handle, AGroupId, G) = AGroupId);
   if Result then begin
      AHeader := G.pszHeader;
   end;
end;

procedure ListViewUpdateGroup(const ListView: TListView; const Header: WideString; const GroupId: integer); inline;
var
   G: TLVGroup;
begin
   ZeroMemory(@G, SizeOf(G));
   G.cbSize := SizeOf(TLVGroup);
   G.mask := LVGF_HEADER; // or LVGF_GROUPID;
   G.pszHeader := pwidechar(Header);
   G.cchHeader := lstrlenW(G.pszHeader);
   G.iGroupId := GroupId;
   ListView_SetGroupInfo(ListView.Handle, GroupId, G);
end;

procedure ListViewSetTileInfo(const AListView: TListView; const ALineCount: integer);
var
   tv: TLVTileViewInfo;
   //ti: TLVTileInfo;
begin
   ZeroMemory(@tv, SizeOf(tv));
   tv.cbSize := SizeOf(TLVTileViewInfo);
   tv.cLines := ALineCount;
   tv.dwMask := LVTVIM_COLUMNS;
   tv.dwFlags := LVTVIF_AUTOSIZE;
   (*
     tv.dwMask := LVTVIM_COLUMNS or LVTVIM_TILESIZE;
     tv.dwFlags := LVTVIF_FIXEDSIZE; // LVTVIF_AUTOSIZE;
     tv.sizeTile.cx := 200;
     tv.sizeTile.cy := 50;
 *)
   ListView_SetTileViewInfo(AListView.Handle, tv);
end;

procedure ListViewSetTileInfo(const AListView: TListView; const ALineCount, ACX, ACY: integer);
var
   tv: TLVTileViewInfo;
begin
   ZeroMemory(@tv, SizeOf(tv));
   tv.cbSize := SizeOf(TLVTileViewInfo);
   tv.cLines := ALineCount;
   tv.dwMask := LVTVIM_COLUMNS;
   tv.dwFlags := LVTVIF_AUTOSIZE;
   tv.dwMask := LVTVIM_COLUMNS or LVTVIM_TILESIZE;
   tv.dwFlags := LVTVIF_FIXEDSIZE; // LVTVIF_AUTOSIZE;
   tv.sizeTile.CX := ACX;
   tv.sizeTile.CY := ACY;
   ListView_SetTileViewInfo(AListView.Handle, tv);
end;

function ListItemGetGroupId(const AListItem: TListItem): integer; inline;
var
   lv: TLVItem60;
begin
   ZeroMemory(@lv, SizeOf(TLVItem60));
   lv.mask := LVIF_GROUPID;
   lv.iItem := AListItem.Index;
   SendMessage(AListItem.ListView.Handle, LVM_GETITEM, 0, lParam(@lv));
   Result := lv.iGroupId;
end;

procedure ListItemSetGroupId(const AListItem: TListItem; const AGroupId: integer);
var
   //lv: TLVItem60;
   item: LVITEMA;
begin
   //ZeroMemory(@lv, SizeOf(TLVItem60));
   //lv.mask := LVIF_GROUPID;
   //lv.iItem := AListItem.Index;
   //lv.iGroupId := AGroupId;
   //SendMessage(AListItem.ListView.Handle, LVM_SETITEM, 0, lParam(@lv));
   ZeroMemory(@item, SizeOf(item));
   item.mask := LVIF_GROUPID;
   item.iItem := AListItem.Index;
   item.iGroupId := AGroupId;
   ListView_SetItem(AListItem.ListView.Handle, item);
end;

procedure ListItemSetMinimumSubItemCount(const AListItem: TListItem; const ACount: integer; const AEmptyFiller: string = ''); inline;
begin
   while AListItem.SubItems.Count < ACount do begin
      AListItem.SubItems.Add(AEmptyFiller);
   end;
end;

procedure ListItemTileViewColumns(const AListItem: TListItem; const AColumnCount: integer; const AColumns: array of integer);
var
   ti: TLVTileInfo;
begin
   ti.cbSize := SizeOf(TLVTileInfo);
   ti.iItem := AListItem.Index;
   ti.cColumns := AColumnCount;
   ti.puColumns := @AColumns[0];
   if not ListView_SetTileInfo(AListItem.ListView.Handle, ti) then begin
      OutputDebugString('[-] ListView_SetTileInfo');
   end;
end;

{$ENDIF MSWindows}

{$IFDEF MSWindows}
{ TLVGroupHelper }

procedure TLVGroupHelper.Init(AGroupID: integer; AMask: UINT);
begin
   ZeroMemory(@Self, SizeOf(Self));
   Self.cbSize := SizeOf(Self);
   Self.mask := AMask;
   Self.iGroupId := AGroupId;
end;

{$ENDIF MSWindows}

{ TListViewGroupWrapper }

{$IFDEF MSWindows}
function TListViewGroupWrapper.GetGroup(AMask: UINT; out AGroup: TLVGroup): boolean;
begin
   AGroup.Init(FGroupID, AMask);
   Result := (ListView_GetGroupInfo(FListView.Handle, FGroupID, AGroup) = FGroupID);
end;

{$ENDIF MSWindows}

function TListViewGroupWrapper.GetHeaderAlignment: TAlignment;
   {$IFDEF MSWindows}
var
   grp: TLVGroup;
   {$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_ALIGN, grp) then begin
      if ((grp.uAlign and LVGA_HEADER_CENTER) > 0) then begin
         Result := taCenter;
      end else if ((grp.uAlign and LVGA_HEADER_RIGHT) > 0) then begin
         Result := taRightJustify;
      end else begin
         Result := taLeftJustify;
      end;
   end else begin
      Result := taLeftJustify;
   end;
   {$ELSE MSWindows}
   Result := taLeftJustify;
   {$ENDIF MSWindows}
end;

function TListViewGroupWrapper.GetSubSeted: boolean;
begin
   {$IFDEF MSWindows}
   Result := GetGroupState(LVGS_SUBSETED, False);
   {$ELSE MSWindows}
   Result := False;
   {$ENDIF MSWindows}
end;

function TListViewGroupWrapper.GetSubSetTitle: WideString;
   {$IFDEF MSWindows}
var
   grp: TLVGroup;
   {$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_SUBSET, grp) then begin
      Result := grp.pszSubtitle;
   end else begin
      Result := '';
   end;
   {$ELSE MSWindows}
   Result := '';
   {$ENDIF MSWindows}
end;

function TListViewGroupWrapper.GetSubTitle: WideString;
   {$IFDEF MSWindows}
var
   grp: TLVGroup;
   {$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_SUBTITLE, grp) then begin
      Result := grp.pszSubtitle;
   end else begin
      Result := '';
   end;
   {$ELSE MSWindows}
   Result := '';
   {$ENDIF MSWindows}
end;

function TListViewGroupWrapper.GetTitleImage: integer;
   {$IFDEF MSWindows}
var
   grp: TLVGroup;
   {$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_TITLEIMAGE, grp) then begin
      Result := grp.iTitleImage;
   end else begin
      Result := -1;
   end;
   {$ELSE MSWindows}
   Result := -1;
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetDescriptionBottom(AValue: WideString);
{$IFDEF MSWindows}
var
   grp: TLVGroup;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_DESCRIPTIONBOTTOM, grp) then begin
      grp.pszDescriptionBottom := pwidechar(AValue);
      ListView_SetGroupInfo(FListView.Handle, FGroupID, grp);
   end;
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetDescriptionTop(AValue: WideString);
{$IFDEF MSWindows}
var
   grp: TLVGroup;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_DESCRIPTIONTOP, grp) then begin
      grp.pszDescriptionTop := pwidechar(AValue);
      ListView_SetGroupInfo(FListView.Handle, FGroupID, grp);
   end;
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetExtendedImage(AValue: integer);
{$IFDEF MSWindows}
var
   grp: TLVGroup;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_EXTENDEDIMAGE, grp) then begin
      grp.iExtendedImage := AValue;
      ListView_SetGroupInfo(FListView.Handle, FGroupID, grp);
   end;
   {$ENDIF MSWindows}
end;

function TListViewGroupWrapper.GetFooter: WideString;
   {$IFDEF MSWindows}
var
   grp: TLVGroup;
   {$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_FOOTER, grp) then begin
      Result := grp.pszFooter;
   end else begin
      Result := '';
   end;
   {$ELSE MSWindows}
   Result := '';
   {$ENDIF MSWindows}
end;

function TListViewGroupWrapper.GetFooterAlignment: TAlignment;
   {$IFDEF MSWindows}
var
   grp: TLVGroup;
   {$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_ALIGN, grp) then begin
      if ((grp.uAlign and LVGA_FOOTER_CENTER) > 0) then begin
         Result := taCenter;
      end else if ((grp.uAlign and LVGA_FOOTER_RIGHT) > 0) then begin
         Result := taRightJustify;
      end else begin
         Result := taLeftJustify;
      end;
   end else begin
      Result := taLeftJustify;
   end;
   {$ELSE MSWindows}
   Result := taLeftJustify;
   {$ENDIF MSWindows}
end;

function TListViewGroupWrapper.GetDescriptionBottom: WideString;
   {$IFDEF MSWindows}
var
   grp: TLVGroup;
   {$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_DESCRIPTIONBOTTOM, grp) then begin
      Result := grp.pszDescriptionBottom;
   end else begin
      Result := '';
   end;
   {$ELSE MSWindows}
   Result := '';
   {$ENDIF MSWindows}
end;

function TListViewGroupWrapper.GetDescriptionTop: WideString;
   {$IFDEF MSWindows}
var
   grp: TLVGroup;
   {$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_DESCRIPTIONTOP, grp) then begin
      Result := grp.pszDescriptionTop;
   end else begin
      Result := '';
   end;
   {$ELSE MSWindows}
   Result := '';
   {$ENDIF MSWindows}
end;

function TListViewGroupWrapper.GetExtendedImage: integer;
   {$IFDEF MSWindows}
var
   grp: TLVGroup;
   {$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_EXTENDEDIMAGE, grp) then begin
      Result := grp.iExtendedImage;
   end else begin
      Result := -1;
   end;
   {$ELSE MSWindows}
   Result := -1;
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetFooter(AValue: WideString);
{$IFDEF MSWindows}
var
   grp: TLVGroup;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_FOOTER, grp) then begin
      grp.pszFooter := pwidechar(AValue);
      ListView_SetGroupInfo(FListView.Handle, FGroupID, grp);
   end;
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetFooterAlignment(AValue: TAlignment);
{$IFDEF MSWindows}
var
   grp: TLVGroup;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_ALIGN, grp) then begin
      case AValue of
         taLeftJustify: begin
            grp.uAlign := (grp.uAlign and (LVGA_HEADER_CENTER or LVGA_HEADER_LEFT or LVGA_HEADER_RIGHT)) or LVGA_FOOTER_LEFT;
         end;
         taRightJustify: begin
            grp.uAlign := (grp.uAlign and (LVGA_HEADER_CENTER or LVGA_HEADER_LEFT or LVGA_HEADER_RIGHT)) or LVGA_FOOTER_RIGHT;
         end;
         taCenter: begin
            grp.uAlign := (grp.uAlign and (LVGA_HEADER_CENTER or LVGA_HEADER_LEFT or LVGA_HEADER_RIGHT)) or LVGA_FOOTER_CENTER;
         end;
      end;
      ListView_SetGroupInfo(FListView.Handle, FGroupID, grp);
   end;
   {$ENDIF MSWindows}
end;

{$IFDEF MSWindows}
function TListViewGroupWrapper.SetGroupState(AState: UINT; ASet: boolean): boolean;
var
   grp: TLVGroup;
begin
   Result := GetGroup(LVGF_STATE, grp);
   if Result then begin
      grp.stateMask := AState;
      if ASet then begin
         grp.state := grp.state or AState;
      end else begin
         grp.state := grp.state and (not AState);
      end;
      ListView_SetGroupInfo(FListView.Handle, FGroupID, grp);
   end;
   Result := False;
end;

function TListViewGroupWrapper.GetGroupState(AState: UINT; ADefault: boolean): boolean;
var
   grp: TLVGroup;
begin
   if GetGroup(LVGF_STATE, grp) then begin
      Result := (grp.state and AState) > 0;
   end else begin
      Result := ADefault;
   end;
end;

{$ENDIF MSWindows}

function TListViewGroupWrapper.GetHeader: WideString;
   {$IFDEF MSWindows}
var
   grp: TLVGroup;
   {$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_HEADER, grp) then begin
      Result := grp.pszHeader;
   end else begin
      Result := '';
   end;
   {$ELSE MSWindows}
   Result := '';
   {$ENDIF MSWindows}
end;

function TListViewGroupWrapper.GetCollapsed: boolean;
begin
   {$IFDEF MSWindows}
   Result := GetGroupState(LVGS_COLLAPSED, False);
   {$ELSE MSWindows}
   Result := False;
   {$ENDIF MSWindows}
end;

function TListViewGroupWrapper.GetCollapsible: boolean;
begin
   {$IFDEF MSWindows}
   Result := GetGroupState(LVGS_COLLAPSIBLE, False);
   {$ELSE MSWindows}
   Result := False;
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetHeader(AValue: WideString);
{$IFDEF MSWindows}
var
   grp: TLVGroup;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   grp.Init(FGroupID, LVGF_HEADER or LVGF_GROUPID);
   grp.pszHeader := pwidechar(AValue);
   ListView_InsertGroup(FListView.Handle, FGroupID, grp);
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetCollapsed(AValue: boolean);
begin
   {$IFDEF MSWindows}
   SetGroupState(LVGS_COLLAPSED, AValue);
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetCollapsible(AValue: boolean);
begin
   {$IFDEF MSWindows}
   SetGroupState(LVGS_COLLAPSIBLE, AValue);
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetHeaderAlignment(AValue: TAlignment);
{$IFDEF MSWindows}
var
   grp: TLVGroup;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_ALIGN, grp) then begin
      case AValue of
         taLeftJustify: begin
            grp.uAlign := (grp.uAlign and (LVGA_FOOTER_CENTER or LVGA_FOOTER_LEFT or LVGA_FOOTER_RIGHT)) or LVGA_HEADER_LEFT;
         end;
         taRightJustify: begin
            grp.uAlign := (grp.uAlign and (LVGA_FOOTER_CENTER or LVGA_FOOTER_LEFT or LVGA_FOOTER_RIGHT)) or LVGA_HEADER_RIGHT;
         end;
         taCenter: begin
            grp.uAlign := (grp.uAlign and (LVGA_FOOTER_CENTER or LVGA_FOOTER_LEFT or LVGA_FOOTER_RIGHT)) or LVGA_HEADER_CENTER;
         end;
      end;
      ListView_SetGroupInfo(FListView.Handle, FGroupID, grp);
   end;
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetSubItemCount(AValue: integer);
begin
   {$IFDEF MSWindows}
   ListView_SetGroupSubItemCount(FListView.Handle, FGroupID, AValue);
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetSubSeted(AValue: boolean);
begin
   {$IFDEF MSWindows}
   SetGroupState(LVGS_SUBSETED, AValue);
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetSubSetTitle(AValue: WideString);
{$IFDEF MSWindows}
var
   grp: TLVGroup;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_SUBSET, grp) then begin
      grp.pszSubtitle := pwidechar(AValue);
      ListView_SetGroupInfo(FListView.Handle, FGroupID, grp);
   end;
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetSubTitle(AValue: WideString);
{$IFDEF MSWindows}
var
   grp: TLVGroup;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_SUBTITLE, grp) then begin
      grp.pszSubtitle := pwidechar(AValue);
      ListView_SetGroupInfo(FListView.Handle, FGroupID, grp);
   end;
   {$ENDIF MSWindows}
end;

procedure TListViewGroupWrapper.SetTitleImage(AValue: integer);
{$IFDEF MSWindows}
var
   grp: TLVGroup;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_TITLEIMAGE, grp) then begin
      grp.iTitleImage := AValue;
      ListView_SetGroupInfo(FListView.Handle, FGroupID, grp);
   end;
   {$ENDIF MSWindows}
end;

constructor TListViewGroupWrapper.Create(AListView: TListView; AGroupID: integer);
begin
   FGroupID := AGroupID;
   FListView := AListView;
end;

procedure TListViewGroupWrapper.SetSubset(ACount: integer; AMore: WideString);
{$IFDEF MSWindows}
var
   grp: TLVGroup;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   if GetGroup(LVGF_STATE or LVGF_SUBSET, grp) then begin
      grp.stateMask := LVGS_SUBSETED;
      grp.state := LVGS_SUBSETED;
      grp.pszSubsetTitle := pwidechar(AMore);
      ListView_SetGroupInfo(FListView.Handle, FGroupID, grp);
      SendMessage(FListView.Handle, LVM_SETGROUPSUBSETCOUNT, FGroupID, ACount);
   end;
   {$ENDIF MSWindows}
end;

{ TListViewGroupingHelper }

procedure TListViewGroupingHelper.AddGroup(const AHeader: WideString; const AGroupId, AImageIndex: integer);
begin
   {$IFNDEF FPC}
   // this is DXE or later
   with Self.Groups.Add do begin
      Header := AHeader;
      GroupId := AGroupId;
      state := [lgsNormal, lgsCollapsible];
   end;
   {$ELSE FPC}
   {$IFDEF MSWindows}
   ListViewAddGroup(Self, AHeader, AGroupId, '', AImageIndex);
   {$ENDIF MSWindows}
   {$ENDIF FPC}
end;

procedure TListViewGroupingHelper.AddColumn(const Header: WideString; AWidth: integer; AAlign: TAlignment; AAutoSize: boolean);
begin
   with Columns.Add do begin
      Caption := string(Header);
      Width := AWidth;
      Alignment := AAlign;
      AutoSize := AAutoSize;
   end;
end;

function TListViewGroupingHelper.GetGroupName(const GroupId: integer): WideString;
begin
   Result := '';
   {$IFDEF MSWindows}
   ListViewGetGroupName(Self, GroupId, Result);
   {$ENDIF MSWindows}
end;

function TListViewGroupingHelper.Group(AID: integer): TListViewGroupWrapper;
begin
   Result := TListViewGroupWrapper.Create(Self, AID);
end;

function TListViewGroupingHelper.GetGroupView: boolean;
begin
   {$IFDEF Unicode}
   Result := Self.GroupView;
   {$ELSE Unicode}
   {$IFDEF MSWindows}
   Result := ListViewGetGroupViewEnabled(Self);
   {$ELSE MSWindows}
   Result := False;
   {$ENDIF MSWindows}
   {$ENDIF Unicode}
end;

procedure TListViewGroupingHelper.SetGroupImages(AValue: TImageList);
begin
   {$IFDEF MSWindows}
   ListView_SetImageList(Self.Handle, AValue.Reference[AValue.Width].Handle, LVSIL_GROUPHEADER);
   {$ENDIF MSWindows}
end;

function TListViewGroupingHelper.GetHeaderInAllViews: boolean;
begin
   {$IFDEF MSWindows}
   Result := ListViewGetHeaderInAllViews(Self);
   {$ELSE MSWindows}
   Result := False;
   {$ENDIF MSWindows}
end;

function TListViewGroupingHelper.GetViewStyleEx: TViewStyleEx;
begin
   {$IFDEF MSWindows}
   case ListView_GetView(Self.Handle) of
      LV_VIEW_ICON: Result := vsIcon;
      LV_VIEW_SMALLICON: Result := vsSmallIcon;
      LV_VIEW_LIST: Result := vsList;
      LV_VIEW_DETAILS: Result := vsReport;
      LV_VIEW_TILE: Result := vsTile;
      else
         Result := vsIcon; // just to please the compiler and future features
   end;
   {$ELSE MSWindows}
   Result := TViewStyleEx(Self.ViewStyle);
   {$ENDIF MSWindows}
end;

procedure TListViewGroupingHelper.SetHeaderInAllViews(const AValue: boolean);
begin
   {$IFDEF MSWindows}
   ListViewSetHeaderInAllViews(Self, AValue);
   {$ENDIF MSWindows}
end;

procedure TListViewGroupingHelper.SetGroupView(const AValue: boolean);
begin
   {$IFNDEF FPC}
   Self.GroupView := True;
   {$ELSE FPC}
   {$IFDEF MSWindows}
   ListViewSetGroupViewEnabled(Self, AValue);
   {$ENDIF MSWindows}
   {$ENDIF FPC}
end;

procedure TListViewGroupingHelper.SetTileViewLines(const AValue: integer);
begin
   {$IFDEF MSWindows}
   ListViewSetTileInfo(Self, AValue);
   {$ENDIF MSWindows}
end;

procedure TListViewGroupingHelper.SetViewStyleEx(const AValue: TViewStyleEx);
begin
   // Double actions for the standard 4 styles is needed since we need
   // Self.ViewStyle for older OS, but the VCL wouldn't know that it needs
   // to switch back on XP or newer.
   {$IFDEF MSWindows}
   case AValue of
      vsIcon:
      begin
         ListView_SetView(Self.Handle, LV_VIEW_ICON);
         Self.ViewStyle := ComCtrls.vsIcon;
      end;
      vsSmallIcon:
      begin
         ListView_SetView(Self.Handle, LV_VIEW_SMALLICON);
         Self.ViewStyle := ComCtrls.vsSmallIcon;
      end;
      vsList:
      begin
         ListView_SetView(Self.Handle, LV_VIEW_LIST);
         Self.ViewStyle := ComCtrls.vsList;
      end;
      vsReport:
      begin
         ListView_SetView(Self.Handle, LV_VIEW_DETAILS);
         Self.ViewStyle := ComCtrls.vsReport;
      end;
      vsTile:
      begin
         Self.ViewStyle := ComCtrls.vsSmallIcon;
         ListView_SetView(Self.Handle, LV_VIEW_TILE);
      end;
   end;
   {$ELSE MSWindows}
   if AValue = vsTile then begin
      Self.ViewStyle := TViewStyle(vsIcon);
   end else begin
      Self.ViewStyle := TViewStyle(AValue);
   end;
   {$ENDIF MSWindows}
end;

procedure TListViewGroupingHelper.UpdateGroup(const AHeader: WideString; const AGroupId: integer);
{$IFNDEF FPC}
var
   i: integer;
{$ENDIF FPC}
begin
   {$IFNDEF FPC}
   // this is DXE or later
   for i := 0 to Pred(Self.Groups.Count) do begin
      if Self.Groups[i].GroupId = AGroupId then begin
         Self.Groups[i].Header := AHeader;
      end;
   end;
   {$ELSE FPC}
   {$IFDEF MSWindows}
   ListViewUpdateGroup(Self, AHeader, AGroupId);
   {$ENDIF MSWindows}
   {$ENDIF FPC}
end;

{ TListItemGroupingHelper }

function TListItemGroupingHelper.GetGroupID: integer;
begin
   {$IFNDEF FPC}
   Result := Self.GroupId;
   {$ELSE FPC}
   {$IFDEF MSWindows}
   Result := ListItemGetGroupId(Self);
   {$ELSE MSWindows}
   Result := 0;
   {$ENDIF MSWindows}
   {$ENDIF FPC}
end;

procedure TListItemGroupingHelper.SetGroupID(const Value: integer);
begin
   {$IFNDEF FPC}
   Self.GroupId := Value;
   {$ELSE FPC}
   {$IFDEF MSWindows}
   ListItemSetGroupId(Self, Value);
   {$ENDIF MSWindows}
   {$ENDIF FPC}
end;

procedure TListItemGroupingHelper.SetMinimumSubItemCount(const ACount: integer; const AEmptyFiller: string);
begin
   {$IFDEF MSWindows}
   ListItemSetMinimumSubItemCount(Self, ACount, AEmptyFiller);
   {$ENDIF MSWindows}
end;

procedure TListItemGroupingHelper.SetTileViewColumns(const AColumnCount: integer; const AColumns: array of integer);
begin
   {$IFDEF MSWindows}
   ListItemTileViewColumns(Self, AColumnCount, AColumns);
   {$ENDIF MSWindows}
end;

{ TObjectListViewClassHelper }

procedure TObjectListViewClassHelper.ObjectDblClick(Sender: TObject);
var
   o: TObject;
   m: TMethod;
begin
   if not Assigned(Self.Selected) then begin
      Exit;
   end;
   o := TObject(Self.Selected.Data);
   try
      m := GetMethodProp(o, 'OnExecute');
      // TODO : execute method
   except
      // no method exists, so what...
   end;
end;

function TObjectListViewClassHelper.HasColumn(AName: string): boolean;
var
   i: integer;
begin
   Result := False;
   for i := 0 to Pred(Self.Columns.Count) do begin
      if SameText(Self.Columns[i].Caption, AName) then begin
         Result := True;
         break;
      end;
   end;
end;

function TObjectListViewClassHelper.FindOrCreateColumn(AName: string): TListColumn;
var
   i: integer;
begin
   for i := 0 to Pred(Self.Columns.Count) do begin
      if SameText(Self.Columns[i].Caption, AName) then begin
         Result := Self.Columns[i];
         Exit;
      end;
   end;
   Result := AddColumn(AName);
end;

function TObjectListViewClassHelper.AddColumn(AName: string): TListColumn;
begin
   Result := Self.Columns.Add;
   Result.Caption := AName;
   Result.AutoSize := True;
   Result.MaxWidth := 300;
   Result.ImageIndex := -1;
end;

procedure TObjectListViewClassHelper.UpdateColumnsForClass(AnItem: TObject);
var
   propList: TPropList;
   iCurrent: integer;
   iCount: integer;
   sName: string;
begin
   iCount := GetPropList(AnItem.ClassInfo, tkAny, @propList, False);
   for iCurrent := 0 to Pred(iCount) do begin
      sName := OVMFieldNameToPublic(propList[iCurrent]^.Name);
      if not Self.HasColumn(sName) then begin
         Self.AddColumn(sName);
      end;
   end;
end;

function TObjectListViewClassHelper.HasPublishedProperties(AnItem: TObject): boolean;
var
   propList: TPropList;
begin
   Result := GetPropList(AnItem.ClassInfo, tkAny, @propList) > 0;
end;

function TObjectListViewClassHelper.UpdateListItemForObject(AListItem: TListItem; AnItem: TObject; AGroups: TStrings; AGroupProperty: string): integer;
var
   sName: string;
   sData: string;
   lc: TListColumn;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   iProperty: integer;
   iAttribute: integer;
   aBoolImageIndex: AOVMBooleanImageIndex;
   attributeEntry: TAttributeEntry;
   attribute: TCustomAttribute;
   aSkip: AListViewSkipField;
   iGroupId: integer;
   iColumn: integer;
   bAdd: boolean;
   colAlignment: TAlignment;
begin
   AListItem.Data := AnItem;
   Result := GetPropList(AnItem.ClassInfo, tkAny, @objectPropertyList, False);
   for iProperty := 0 to Pred(Result) do begin
      try
         colAlignment := taLeftJustify;
         objectPropertyInfo := objectPropertyList[iProperty];
         if (Assigned(AGroups)) and (SameText(AGroupProperty, objectPropertyInfo^.Name)) then begin
            sData := OVMFieldNameToPublic(ObjectPropertyToDisplayText(AnItem, objectPropertyInfo));
            iGroupId := AGroups.IndexOf(sData);
            if (iGroupId < 0) then begin
               iGroupId := AGroups.Add(sData);
               with Self.Group(iGroupId) do begin
                  Header := WideString(sData);
                  Free;
               end;
            end;
            AListItem.GroupId := iGroupId;
         end else if (objectPropertyInfo^.Name = 'GroupId') then begin
            AListItem.GroupId := GetInt64Prop(AnItem, objectPropertyInfo);
         end else begin
            sName := OVMFieldNameToPublic(objectPropertyInfo^.Name);
            bAdd := True;
            if Assigned(objectPropertyInfo^.AttributeTable) then begin
               for iAttribute := 0 to Pred(objectPropertyInfo^.AttributeTable.AttributeCount) do begin
                  attributeEntry := objectPropertyInfo^.AttributeTable.AttributesList[iAttribute];
                  attribute := attributeEntry.AttrProc;
                  if attribute is AListViewColumnDetails then begin
                     sName := (attribute as AListViewColumnDetails).ColumnName;
                     colAlignment := (attribute as AListViewColumnDetails).Alignment;
                  end else if attribute is AOVMImageIndex then begin
                     AListItem.ImageIndex := GetOrdProp(AnItem, objectPropertyInfo);
                     bAdd := False;
                  end else if attribute is AOVMBooleanImageIndex then begin
                     aBoolImageIndex := AOVMBooleanImageIndex(attribute);
                     if (1 = GetInt64Prop(AnItem, objectPropertyInfo)) then begin
                        AListItem.ImageIndex := aBoolImageIndex.ImageIndexTrue;
                     end else begin
                        AListItem.ImageIndex := aBoolImageIndex.ImageIndexFalse;
                     end;
                     bAdd := False;
                  end;
               end;
            end;
            if objectPropertyInfo^.PropType^.Kind in [tkMethod, tkClass] then begin
               bAdd := False;
            end;
            aSkip := AListViewSkipField(GetPropAttributeFromTypInfo(objectPropertyInfo, AListViewSkipField));
            if Assigned(aSkip) then begin
               bAdd := False;
            end;
            if bAdd then begin
               lc := Self.FindOrCreateColumn(sName);
               if (lc.Alignment <> colAlignment) then begin
                  lc.Alignment := colAlignment;
               end;
               sData := ObjectPropertyToDisplayText(AnItem, objectPropertyInfo);
               if (lc.Index = 0) then begin
                  AListItem.Caption := sData;
               end else begin
                  while AListItem.SubItems.Count < lc.Index do begin
                     AListItem.SubItems.Add('');
                  end;
                  iColumn := lc.Index - 1;
                  AListItem.SubItems[iColumn] := Trim(sData);
               end;
            end;
         end;
      except
         on E: Exception do begin
            DebugLogger.LogException(E, Format('TObjectListViewClassHelper.UpdateListItemForObject(x, %s)', [AnItem.ClassName]));
         end;
      end;
   end;
end;

function TObjectListViewClassHelper.AddToPropertyList(AnItem: TObject; AGroupID: integer): integer;
var
   propList: TPropList;
   pProp: PPropInfo;
   iCurrent: integer;
   sName: string;
   sData: string;
   li: TListItem;
begin
   Result := GetPropList(AnItem.ClassInfo, tkAny, @propList, False);
   for iCurrent := 0 to Pred(Result) do begin
      pProp := propList[iCurrent];
      sName := OVMFieldNameToPublic(pProp^.Name);
      sData := ObjectPropertyToDisplayText(AnItem, pProp);
      li := Self.Items.Add;
      li.Caption := sName;
      li.SubItems.Add(sData);
      li.GroupId := AGroupID;
   end;
end;

procedure TObjectListViewClassHelper.DisplayItems<T>(TheItems: TEnumerable<T>);
var
   item: T;
   li: TListItem;
   slGroups: TStringList;
   sGroupProperty: string;
   aGrouping: AListViewGrouping;
   ti: PTypeInfo;
   selectedItem: T;
begin
   if not Assigned(Self.OnDblClick) then begin
      Self.OnDblClick := Self.ObjectDblClick;
   end;
   ti := TypeInfo(T);
   aGrouping := AListViewGrouping(GetTypeAttributeFromTypInfo(ti, AListViewGrouping));
   if Assigned(aGrouping) then begin
      sGroupProperty := aGrouping.GroupProperty;
      Self.GroupView := aGrouping.GroupView;
      slGroups := TStringList.Create;
   end else begin
      sGroupProperty := '';
      Self.GroupView := False;
      slGroups := nil;
   end;
   selectedItem := Self.GetSelectedItem<T>();
   try
      Self.Items.BeginUpdate;
      try
         Self.Items.Clear;
         Self.Columns.Clear;
         for item in TheItems do begin
            li := Self.Items.Add;
            Self.UpdateListItemForObject(li, item, slGroups, sGroupProperty);
         end;
      finally
         Self.Items.EndUpdate;
      end;
   finally
      slGroups.Free;
      Self.SetSelectedItem<T>(selectedItem);
   end;
end;

procedure TObjectListViewClassHelper.DisplayItems<T>(AnEnumerator: TEnumerator<T>);
var
   item: T;
   li: TListItem;
   slGroups: TStringList;
   sGroupProperty: string;
   aGrouping: AListViewGrouping;
   ti: PTypeInfo;
   selectedItem: T;
begin
   if not Assigned(Self.OnDblClick) then begin
      Self.OnDblClick := Self.ObjectDblClick;
   end;
   ti := TypeInfo(T);
   aGrouping := AListViewGrouping(GetTypeAttributeFromTypInfo(ti, AListViewGrouping));
   if Assigned(aGrouping) then begin
      sGroupProperty := aGrouping.GroupProperty;
      Self.GroupView := aGrouping.GroupView;
      slGroups := TStringList.Create;
   end else begin
      sGroupProperty := '';
      Self.GroupView := False;
      slGroups := nil;
   end;
   selectedItem := Self.GetSelectedItem<T>();
   try
      Self.Items.BeginUpdate;
      try
         Self.Items.Clear;
         Self.Columns.Clear;
         try
            // some enumerators start with -1, some with the first item
            item := AnEnumerator.Current;
            if Assigned(item) then begin
               li := Self.Items.Add;
               Self.UpdateListItemForObject(li, item, slGroups, sGroupProperty);
            end;
         except
         end;
         while AnEnumerator.MoveNext do begin
            item := AnEnumerator.Current;
            li := Self.Items.Add;
            Self.UpdateListItemForObject(li, item, slGroups, sGroupProperty);
         end;
      finally
         Self.Items.EndUpdate;
      end;
   finally
      slGroups.Free;
      Self.SetSelectedItem<T>(selectedItem);
   end;
end;

procedure TObjectListViewClassHelper.DisplayItems(AList: TCollection);
var
   ci: TCollectionItem;
   li: TListItem;
   slGroups: TStringList;
   sGroupProperty: string;
   a: AListViewGrouping;
   ti: PTypeInfo;
   selectedItem: TCollectionITem;
begin
   if not Assigned(Self.OnDblClick) then begin
      Self.OnDblClick := Self.ObjectDblClick;
   end;
   ti := PTypeInfo(AList.ItemClass.ClassInfo);
   a := AListViewGrouping(GetTypeAttributeFromTypInfo(ti, AListViewGrouping));
   if Assigned(a) then begin
      sGroupProperty := a.GroupProperty;
      Self.GroupView := a.GroupView;
      slGroups := TStringList.Create;
   end else begin
      sGroupProperty := '';
      Self.GroupView := False;
      slGroups := nil;
   end;
   selectedItem := Self.GetSelectedItem<TCollectionItem>();
   try
      Self.Items.BeginUpdate;
      try
         Self.Items.Clear;
         Self.Columns.Clear;
         for ci in AList do begin
            li := Self.Items.Add;
            Self.UpdateListItemForObject(li, ci, slGroups, sGroupProperty);
         end;
      finally
         Self.Items.EndUpdate;
      end;
   finally
      slGroups.Free;
      Self.SetSelectedItem<TCollectionItem>(selectedItem);
   end;
end;

procedure TObjectListViewClassHelper.DisplayProperties(AnItem: TObject);

   procedure UpdateImageIndizes(AListItem: TListItem; AnItem: TObject; AProperty: PPropInfo);
   var
      aImageIndex: APropertyListViewBooleanValueItemIndexes;
   begin
      aImageIndex := APropertyListViewBooleanValueItemIndexes(GetPropAttributeFromTypInfo(AProperty, APropertyListViewBooleanValueItemIndexes));
      if Assigned(aImageIndex) then begin
         if (GetInt64Prop(AnItem, AProperty) = 1) then begin
            AListItem.SubItemImages[0] := aImageIndex.ImageIndexTrue;
         end else begin
            AListItem.SubItemImages[0] := aImageIndex.ImageIndexFalse;
         end;
      end;
      aImageIndex := APropertyListViewBooleanValueItemIndexes(GetPropAttributeFromTypInfo(AProperty, APropertyListViewBooleanKeyItemIndexes));
      if Assigned(aImageIndex) then begin
         if (GetInt64Prop(AnItem, AProperty) = 1) then begin
            AListItem.ImageIndex := aImageIndex.ImageIndexTrue;
         end else begin
            AListItem.ImageIndex := aImageIndex.ImageIndexFalse;
         end;
      end;
   end;

   procedure UpdateBooleanText(AnItem: TObject; AProperty: PPropInfo; var AText: string);
   var
      a: AOVMBooleanStringValues;
      ayn: AOVMBooleanYesNo;
   begin
      a := AOVMBooleanStringValues(GetPropAttributeFromTypInfo(AProperty, AOVMBooleanStringValues));
      if Assigned(a) and (AProperty^.PropType^.Kind = tkBool) then begin
         if (GetInt64Prop(AnItem, AProperty) = 1) then begin
            AText := a.TextTrue;
         end else begin
            AText := a.TextFalse;
         end;
      end;
      ayn := AOVMBooleanYesNo(GetPropAttributeFromTypInfo(AProperty, AOVMBooleanYesNo));
      if Assigned(ayn) and (AProperty^.PropType^.Kind = tkBool) then begin
         if (GetInt64Prop(AnItem, AProperty) = 1) then begin
            AText := ayn.TextTrue;
         end else begin
            AText := ayn.TextFalse;
         end;
      end;
   end;

   function GetGroupIndex(AList: TStrings; AProperty: PPropInfo): integer;
   var
      aGroup: APropertyListViewGroup;
      g: TListViewGroupWrapper;
   begin
      Result := 0;
      aGroup := APropertyListViewGroup(GetPropAttributeFromTypInfo(AProperty, APropertyListViewGroup));
      if Assigned(aGroup) then begin
         Result := AList.IndexOf(aGroup.GroupName);
         if (Result < 0) then begin
            Result := AList.Add(aGroup.GroupName);
            g := Self.Group(Result);
            try
               g.Header := WideString(aGroup.GroupName);
            finally
               g.Free;
            end;
         end;
      end;
   end;

var
   slGroups: TStringList;
   propList: TPropList;
   pProp: PPropInfo;
   iCurrent: integer;
   iGroup: integer;
   sName: string;
   sData: string;
   li: TListItem;
   iPropertyCount: integer;
   aKeyName: APropertyListViewKeyName;
   aSkipEmpty: APropertyListViewSkipIfEmpty;
   aSkipZero: APropertyListViewSkipIfZero;
   aParentO: ATreeViewParentObjectField;
   aImageIndex: AOVMImageIndex;
   aButton: AOVMButton;
   bAdd: boolean;
begin
   iPropertyCount := GetPropList(AnItem.ClassInfo, tkAny, @propList, False);
   slGroups := TStringList.Create;
   iGroup := slGroups.Add('Properties');
   {$IFDEF MSWindows}
   ListViewAddGroup(Self, 'Properties', iGroup);
   {$ENDIF MSWindows}
   Self.GroupView := True;
   Self.Items.BeginUpdate;
   try
      try
         Self.Items.Clear;
         for iCurrent := 0 to Pred(iPropertyCount) do begin
            pProp := propList[iCurrent];
            sName := OVMFieldNameToPublic(pProp^.Name);
            sData := Trim(ObjectPropertyToDisplayText(AnItem, pProp));
            UpdateBooleanText(AnItem, pProp, sData);
            iGroup := GetGroupIndex(slGroups, pProp);
            aKeyName := APropertyListViewKeyName(GetPropAttributeFromTypInfo(pProp, APropertyListViewKeyName));
            if Assigned(aKeyName) then begin
               sName := aKeyName.KeyName;
            end;
            bAdd := True;
            aSkipEmpty := APropertyListViewSkipIfEmpty(GetPropAttributeFromTypInfo(pProp, APropertyListViewSkipIfEmpty));
            if Assigned(aSkipEmpty) and (Length(sData) = 0) then begin
               bAdd := False;
            end;
            aSkipZero := APropertyListViewSkipIfZero(GetPropAttributeFromTypInfo(pProp, APropertyListViewSkipIfZero));
            if Assigned(aSkipZero) and (sData = '0') then begin
               bAdd := False;
            end;
            aParentO := ATreeViewParentObjectField(GetPropAttributeFromTypInfo(pProp, ATreeViewParentObjectField));
            if Assigned(aParentO) then begin
               bAdd := False;
            end;
            aImageIndex := AOVMImageIndex(GetPropAttributeFromTypInfo(pProp, AOVMImageIndex));
            if Assigned(aImageIndex) then begin
               bAdd := False;
            end;
            aButton := AOVMButton(GetPropAttributeFromTypInfo(pProp, AOVMButton));
            if Assigned(aButton) then begin
               bAdd := False;
            end;
            if tkMethod = pProp^.PropType^.Kind then begin
               bAdd := False;
            end;

            if bAdd then begin
               li := Self.Items.Add;
               li.Caption := sName;
               li.SubItems.Add(sData);
               li.GroupId := iGroup;
               UpdateImageIndizes(li, AnItem, pProp);
            end;
         end;
      finally
         Self.Items.EndUpdate;
      end;
   finally
      slGroups.Free;
   end;
end;

function TObjectListViewClassHelper.GetSelectedItem<T>: T;
begin
   Result := nil;
   if not Assigned(Self.Selected) then begin
      Exit;
   end;
   Result := T(Self.Selected.Data);
end;

procedure TObjectListViewClassHelper.SetSelectedItem<T>(AnItem: T);
var
   li: TListItem;
   i: integer;
begin
   if not Assigned(AnItem) then begin
      Exit;
   end;
   for i := 0 to Pred(Self.Items.Count) do begin
      li := Self.Items[i];
      if T(li.Data) = AnItem then begin
         Self.Selected := li;
         Self.ItemFocused := li;
         Exit;
      end;
   end;
end;

function WindowsSupportsListViewGroups: boolean;
begin
   {$IFDEF MSWindows}
   Result := GIsWindowsXPOrLater;
   {$ELSE MSWindows}
   Result := False;
   {$ENDIF MSWindows}
end;

begin
   GIsWindowsXPOrLater := MyWindowsIsXPOrLater;

end.
