{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Constants used by Windows ListViews and TreeViews.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2000-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-02-22  pk    5m  [CCD] Header update
// 2017-02-22  pk    5m  [CCD] Renamed from snlUIWinAPIConsts
// *****************************************************************************
   )
}

unit OVM.Windows.Consts;

interface

uses
   {$IFDEF MSWindows}
   Windows,
   CommCtrl,
   {$ENDIF MSWindows}
   Messages;


const
   LVM_FIRST               = $1000; { ListView messages }
   LVM_GETITEMA            = LVM_FIRST + 5;
   LVM_SETITEMA            = LVM_FIRST + 6;
   LVM_INSERTITEMA         = LVM_FIRST + 7;
   LVM_SETCOLUMNWIDTH      = LVM_FIRST + 30;
   LVM_GETITEMW            = LVM_FIRST + 75;
   LVM_SETITEMW            = LVM_FIRST + 76;
   LVM_INSERTITEMW         = LVM_FIRST + 77;
   LVM_SETITEM             = LVM_SETITEMA;
   LVM_INSERTITEM          = LVM_INSERTITEMA;
   LVM_SETGROUPSUBSETCOUNT = LVM_FIRST + 190;
   LVM_GETGROUPSUBSETCOUNT = LVM_FIRST + 191;
  {$EXTERNALSYM LVM_INSERTGROUP}
   LVM_INSERTGROUP         = LVM_FIRST + 145;
  {$EXTERNALSYM LVM_SETGROUPINFO}
   LVM_SETGROUPINFO        = LVM_FIRST + 147;
  {$EXTERNALSYM LVM_GETGROUPINFO}
   LVM_GETGROUPINFO        = LVM_FIRST + 149;
  {$EXTERNALSYM LVM_REMOVEGROUP}
   LVM_REMOVEGROUP         = LVM_FIRST + 150;
  {$EXTERNALSYM LVM_MOVEGROUP}
   LVM_MOVEGROUP           = LVM_FIRST + 151;
  {$EXTERNALSYM LVM_MOVEITEMTOGROUP}
   LVM_MOVEITEMTOGROUP     = LVM_FIRST + 154;
   {$EXTERNALSYM LVM_SETGROUPMETRICS}
   LVM_SETGROUPMETRICS     = LVM_FIRST + 155;
   {$EXTERNALSYM LVM_GETGROUPMETRICS}
   LVM_GETGROUPMETRICS     = LVM_FIRST + 156;
   {$EXTERNALSYM LVM_ENABLEGROUPVIEW}
   LVM_ENABLEGROUPVIEW     = LVM_FIRST + 157;
   {$EXTERNALSYM LVM_SORTGROUPS}
   LVM_SORTGROUPS          = LVM_FIRST + 158;
   {$EXTERNALSYM LVM_INSERTGROUPSORTED}
   LVM_INSERTGROUPSORTED   = LVM_FIRST + 159;
   {$EXTERNALSYM LVM_REMOVEALLGROUPS}
   LVM_REMOVEALLGROUPS     = LVM_FIRST + 160;
 {$EXTERNALSYM LVM_HASGROUP}
   LVM_HASGROUP            = LVM_FIRST + 161;
   {$EXTERNALSYM LVM_SETTILEVIEWINFO}
   LVM_SETTILEVIEWINFO     = LVM_FIRST + 162;
   {$EXTERNALSYM LVM_GETTILEVIEWINFO}
   LVM_GETTILEVIEWINFO     = LVM_FIRST + 163;
   {$EXTERNALSYM LVM_SETTILEINFO}
   LVM_SETTILEINFO         = LVM_FIRST + 164;
   {$EXTERNALSYM LVM_GETTILEINFO}
   LVM_GETTILEINFO         = LVM_FIRST + 165;
   {$EXTERNALSYM LVIM_AFTER}
   LVIM_AFTER              = $00000001; // TRUE = insert After iItem, otherwise before
   {$EXTERNALSYM LVM_SETINSERTMARK}
   LVM_SETINSERTMARK       = LVM_FIRST + 166;
   {$EXTERNALSYM LVM_GETINSERTMARK}
   LVM_GETINSERTMARK       = LVM_FIRST + 167;
   {$EXTERNALSYM LVM_INSERTMARKHITTEST}
   LVM_INSERTMARKHITTEST   = LVM_FIRST + 168;
   {$EXTERNALSYM LVM_GETINSERTMARKRECT}
   LVM_GETINSERTMARKRECT   = LVM_FIRST + 169;
   {$EXTERNALSYM LVM_SETINSERTMARKCOLOR}
   LVM_SETINSERTMARKCOLOR  = LVM_FIRST + 170;
   {$EXTERNALSYM LVM_GETINSERTMARKCOLOR}
   LVM_GETINSERTMARKCOLOR  = LVM_FIRST + 171;
   {$EXTERNALSYM LVM_SETINFOTIP}
   LVM_SETINFOTIP          = LVM_FIRST + 173;
   {$EXTERNALSYM LVM_GETSELECTEDCOLUMN}
   LVM_GETSELECTEDCOLUMN   = LVM_FIRST + 174;
   {$EXTERNALSYM LVM_ISGROUPVIEWENABLED}
   LVM_ISGROUPVIEWENABLED  = LVM_FIRST + 175;
   {$EXTERNALSYM LVM_GETOUTLINECOLOR}
   LVM_GETOUTLINECOLOR     = LVM_FIRST + 176;
    {$EXTERNALSYM LVM_SETOUTLINECOLOR}
   LVM_SETOUTLINECOLOR     = LVM_FIRST + 177;
   {$EXTERNALSYM LVM_CANCELEDITLABEL}
   LVM_CANCELEDITLABEL     = LVM_FIRST + 179;
      {$EXTERNALSYM LVM_MAPINDEXTOID}
   LVM_MAPINDEXTOID        = LVM_FIRST + 180;
   {$EXTERNALSYM LVM_MAPIDTOINDEX}
   LVM_MAPIDTOINDEX        = LVM_FIRST + 181;

const
  {$EXTERNALSYM LVGMF_NONE}
   LVGMF_NONE        = $00000000;
  {$EXTERNALSYM LVGMF_BORDERSIZE}
   LVGMF_BORDERSIZE  = $00000001;
  {$EXTERNALSYM LVGMF_BORDERCOLOR}
   LVGMF_BORDERCOLOR = $00000002;
  {$EXTERNALSYM LVGMF_TEXTCOLOR}
   LVGMF_TEXTCOLOR   = $00000004;


   LVIF_TEXT  = $0001;
   LVIF_IMAGE = $0002;
   LVCF_TEXT  = $0004;
   LVCF_FMT   = $0001;

   TV_FIRST          = $1100; { TreeView messages }
   TVIF_TEXT         = $0001;
   TVM_INSERTITEMA   = TV_FIRST + 0;
   TVM_INSERTITEMW   = TV_FIRST + 50;
   LVM_INSERTCOLUMNA = LVM_FIRST + 27;
   LVM_INSERTCOLUMNW = LVM_FIRST + 97;
   TVM_INSERTITEM    = TVM_INSERTITEMA;
   LVM_INSERTCOLUMN  = LVM_INSERTCOLUMNA;

   LVCFMT_LEFT        = $0000;
   LVCFMT_RIGHT       = $0001;
   LVCFMT_CENTER      = $0002;
   LVCFMT_JUSTIFYMASK = $0003;

   {$IFDEF MSWindows}
   SB_SETPARTS = WM_USER + 4;
   SB_GETPARTS = WM_USER + 6;
   {$ENDIF MSWindows}

  {$EXTERNALSYM TBSTYLE_BUTTON}
   TBSTYLE_BUTTON     = $00;
  {$EXTERNALSYM TBSTYLE_SEP}
   TBSTYLE_SEP        = $01;
  {$EXTERNALSYM TBSTYLE_CHECK}
   TBSTYLE_CHECK      = $02;
  {$EXTERNALSYM TBSTYLE_GROUP}
   TBSTYLE_GROUP      = $04;
  {$EXTERNALSYM TBSTYLE_CHECKGROUP}
   TBSTYLE_CHECKGROUP = TBSTYLE_GROUP or TBSTYLE_CHECK;
  {$EXTERNALSYM TBSTYLE_DROPDOWN}
   TBSTYLE_DROPDOWN   = $08;
  {$EXTERNALSYM TBSTYLE_AUTOSIZE}
   TBSTYLE_AUTOSIZE   = $0010; // automatically calculate the cx of the button
  {$EXTERNALSYM TBSTYLE_NOPREFIX}
   TBSTYLE_NOPREFIX   = $0020; // if this button should not have accel prefix

  {$EXTERNALSYM TBSTYLE_TOOLTIPS}
   TBSTYLE_TOOLTIPS        = $0100;
  {$EXTERNALSYM TBSTYLE_WRAPABLE}
   TBSTYLE_WRAPABLE        = $0200;
  {$EXTERNALSYM TBSTYLE_ALTDRAG}
   TBSTYLE_ALTDRAG         = $0400;
  {$EXTERNALSYM TBSTYLE_FLAT}
   TBSTYLE_FLAT            = $0800;
  {$EXTERNALSYM TBSTYLE_LIST}
   TBSTYLE_LIST            = $1000;
  {$EXTERNALSYM TBSTYLE_CUSTOMERASE}
   TBSTYLE_CUSTOMERASE     = $2000;
  {$EXTERNALSYM TBSTYLE_REGISTERDROP}
   TBSTYLE_REGISTERDROP    = $4000;
  {$EXTERNALSYM TBSTYLE_TRANSPARENT}
   TBSTYLE_TRANSPARENT     = $8000;
  {$EXTERNALSYM TBSTYLE_EX_DRAWDDARROWS}
   TBSTYLE_EX_DRAWDDARROWS = $00000001;

const
  {$EXTERNALSYM LVTVIF_AUTOSIZE}
   LVTVIF_AUTOSIZE    = $00000000;
  {$EXTERNALSYM LVTVIF_FIXEDWIDTH}
   LVTVIF_FIXEDWIDTH  = $00000001;
  {$EXTERNALSYM LVTVIF_FIXEDHEIGHT}
   LVTVIF_FIXEDHEIGHT = $00000002;
  {$EXTERNALSYM LVTVIF_FIXEDSIZE}
   LVTVIF_FIXEDSIZE   = $00000003;
  {$EXTERNALSYM LVTVIM_TILESIZE}
   LVTVIM_TILESIZE    = $00000001;
  {$EXTERNALSYM LVTVIM_COLUMNS}
   LVTVIM_COLUMNS     = $00000002;
  {$EXTERNALSYM LVTVIM_LABELMARGIN}
   LVTVIM_LABELMARGIN = $00000004;

const
  {$EXTERNALSYM BTNS_BUTTON}
   BTNS_BUTTON             = TBSTYLE_BUTTON;      // 0x0000
  {$EXTERNALSYM BTNS_SEP}
   BTNS_SEP                = TBSTYLE_SEP;         // 0x0001
  {$EXTERNALSYM BTNS_CHECK}
   BTNS_CHECK              = TBSTYLE_CHECK;       // 0x0002
  {$EXTERNALSYM BTNS_GROUP}
   BTNS_GROUP              = TBSTYLE_GROUP;       // 0x0004
  {$EXTERNALSYM BTNS_CHECKGROUP}
   BTNS_CHECKGROUP         = TBSTYLE_CHECKGROUP;  // (TBSTYLE_GROUP | TBSTYLE_CHECK)
  {$EXTERNALSYM BTNS_DROPDOWN}
   BTNS_DROPDOWN           = TBSTYLE_DROPDOWN;    // 0x0008
  {$EXTERNALSYM BTNS_AUTOSIZE}
   BTNS_AUTOSIZE           = TBSTYLE_AUTOSIZE;    // 0x0010; automatically calculate the cx of the button
  {$EXTERNALSYM BTNS_NOPREFIX}
   BTNS_NOPREFIX           = TBSTYLE_NOPREFIX;    // 0x0020; this button should not have accel prefix
  {$EXTERNALSYM BTNS_SHOWTEXT}
   BTNS_SHOWTEXT           = $0040;               // ignored unless TBSTYLE_EX_MIXEDBUTTONS is set
  {$EXTERNALSYM BTNS_WHOLEDROPDOWN}
   BTNS_WHOLEDROPDOWN      = $0080;               // draw drop-down arrow, but without split arrow section
  {$EXTERNALSYM TBSTYLE_EX_MIXEDBUTTONS}
   TBSTYLE_EX_MIXEDBUTTONS = $00000008;
  {$EXTERNALSYM TBSTYLE_EX_HIDECLIPPEDBUTTONS}
   TBSTYLE_EX_HIDECLIPPEDBUTTONS = $00000010;  // don't show partially obscured buttons
  {$EXTERNALSYM TBSTYLE_EX_DOUBLEBUFFER}
   TBSTYLE_EX_DOUBLEBUFFER = $00000080; // Double Buffer the toolbar

  {$EXTERNALSYM TBN_FIRST}
   TBN_FIRST = 0 - 700;       { toolbar }
  {$EXTERNALSYM TBN_LAST}
   TBN_LAST  = 0 - 720;

  {$EXTERNALSYM TBN_RESTORE}
   TBN_RESTORE        = TBN_FIRST - 21;
  {$EXTERNALSYM TBN_SAVE}
   TBN_SAVE           = TBN_FIRST - 22;
  {$EXTERNALSYM TBN_INITCUSTOMIZE}
   TBN_INITCUSTOMIZE  = TBN_FIRST - 23;
  {$EXTERNALSYM TBNRF_HIDEHELP}
   TBNRF_HIDEHELP     = $00000001;
  {$EXTERNALSYM TBNRF_ENDCUSTOMIZE}
   TBNRF_ENDCUSTOMIZE = $00000002;


(********************************
   Tooltipps
********************************)
const
  {$EXTERNALSYM TTS_NOFADE}
   TTS_NOFADE  = $20;
  {$EXTERNALSYM TTS_BALLOON}
   TTS_BALLOON = $40;
  {$EXTERNALSYM TTS_CLOSE}
   TTS_CLOSE   = $80;

   // ToolTip Icons (Set with TTM_SETTITLE)
  {$EXTERNALSYM TTI_NONE}
   TTI_NONE    = 0;
  {$EXTERNALSYM TTI_INFO}
   TTI_INFO    = 1;
  {$EXTERNALSYM TTI_WARNING}
   TTI_WARNING = 2;
  {$EXTERNALSYM TTI_ERROR}
   TTI_ERROR   = 3;

   {$IFDEF MSWindows}
  {$EXTERNALSYM TTM_GETBUBBLESIZE}
   TTM_GETBUBBLESIZE = WM_USER + 30;
  {$EXTERNALSYM TTM_ADJUSTRECT}
   TTM_ADJUSTRECT    = WM_USER + 31;
  {$EXTERNALSYM TTM_SETTITLEA}
   TTM_SETTITLEA     = WM_USER + 32;  // wParam = TTI_*, lParam = char* szTitle
  {$EXTERNALSYM TTM_SETTITLEW}
   TTM_SETTITLEW     = WM_USER + 33;  // wParam = TTI_*, lParam = wchar* szTitle
  {$EXTERNALSYM TTM_POPUP}
   TTM_POPUP         = WM_USER + 34;
  {$EXTERNALSYM TTM_GETTITLE}
   TTM_GETTITLE      = WM_USER + 35; // wParam = 0, lParam = TTGETTITLE*
  {$EXTERNALSYM TTM_SETTITLE}
   TTM_SETTITLE      = TTM_SETTITLEA;
   {$ENDIF MSWindows}

{$IFDEF MSWindows}
type
   _TGETTITLE = packed record
      dwSize: DWORD;
      uTitleBitmap: UINT;
      cch: UINT;
      pszTitle: pwidechar;
   end;
   TGetTitle = _TGETTITLE;
   PGetTitle = ^TGetTitle;
   {$ENDIF MSWindows}


(********************************
   Listview
********************************)

const
  {$EXTERNALSYM LVS_EX_LABELTIP}
   LVS_EX_LABELTIP     = $00004000; // listview unfolds partly hidden labels if it does not have infotip text
  {$EXTERNALSYM LVS_EX_BORDERSELECT}
   LVS_EX_BORDERSELECT = $00008000; // border selection style instead of highlight
  {$EXTERNALSYM LVS_EX_DOUBLEBUFFER}
   LVS_EX_DOUBLEBUFFER = $00010000;
  {$EXTERNALSYM LVS_EX_HIDELABELS}
   LVS_EX_HIDELABELS   = $00020000;
  {$EXTERNALSYM LVS_EX_SINGLEROW}
   LVS_EX_SINGLEROW    = $00040000;
  {$EXTERNALSYM LVS_EX_SNAPTOGRID}
   LVS_EX_SNAPTOGRID   = $00080000;  // Icons automatically snap to grid.
  {$EXTERNALSYM LVS_EX_SIMPLESELECT}
   LVS_EX_SIMPLESELECT = $00100000;  // Also changes overlay rendering to top right for icon mode.

   LVS_EX_HEADERINALLVIEWS = $02000000;

const
   {$EXTERNALSYM LVIF_GROUPID}
   LVIF_GROUPID = $0100;
   {$EXTERNALSYM LVIF_COLUMNS}
   LVIF_COLUMNS = $0200;
   {$EXTERNALSYM LVIS_GLOW}
   LVIS_GLOW    = $0010;

   LVGS_SUBSETED = $0040;
   LVGF_SUBSET   = $8000;


{$IFDEF MSWindows}
type
   {$IFDEF FPC}
   tagLVITEMW = record
      mask: UINT;
      iItem: longint;
      iSubItem: longint;
      state: UINT;
      stateMask: UINT;
      pszText: LPWSTR;
      cchTextMax: longint;
      iImage: longint;
      lParam: LPARAM;
      iIndent: longint;
      iGroupId: longint;
      cColumns: UINT;          // tile view columns
      puColumns: PUINT;
   end;
   PLVItemA = TLVItem;
   TLVItemA = TLVItem;
   TLVItemW = tagLVITEMW;
   //TLVItemW = TLVItem;
   //PFNLVCOMPARE = function(lParam1, lParam2, lParamSort: Integer): Integer stdcall;
   //TLVCompare = PFNLVCOMPARE;
   {$ENDIF FPC}

   PLVItem60A = ^TLVItemA;
   PLVItem60W = ^TLVItemW;
   PLVItem60 = PLVItemA;
  {$EXTERNALSYM tagLVITEM60A}
   tagLVITEM60A = packed record
      mask: UINT;
      iItem: integer;
      iSubItem: integer;
      state: UINT;
      stateMask: UINT;
      pszText: pansichar;
      cchTextMax: integer;
      iImage: integer;
      lParam: LPARAM;
      iIndent: integer;
      iGroupId: integer;
      cColumns: uint;
      puColumns: PUINT;
   end;
  {$EXTERNALSYM tagLVITEM60W}
   tagLVITEM60W = packed record
      mask: UINT;
      iItem: integer;
      iSubItem: integer;
      state: UINT;
      stateMask: UINT;
      pszText: pwidechar;
      cchTextMax: integer;
      iImage: integer;
      lParam: LPARAM;
      iIndent: integer;
      iGroupId: integer;
      cColumns: uint;
      puColumns: PUINT;
   end;
  {$EXTERNALSYM tagLVITEM60}
   tagLVITEM60 = tagLVITEM60A;
  {$EXTERNALSYM _LV_ITEM60A}
   _LV_ITEM60A = tagLVITEM60A;
  {$EXTERNALSYM _LV_ITEM60W}
   _LV_ITEM60W = tagLVITEM60W;
  {$EXTERNALSYM _LV_ITEM60}
   _LV_ITEM60 = _LV_ITEM60A;
   TLVItem60A = tagLVITEM60A;
   TLVItem60W = tagLVITEM60W;
   TLVItem60 = TLVItem60A;
  {$EXTERNALSYM LV_ITEM60A}
   LV_ITEM60A = tagLVITEM60A;
  {$EXTERNALSYM LV_ITEM60W}
   LV_ITEM60W = tagLVITEM60W;
  {$EXTERNALSYM LV_ITEM60}
   LV_ITEM60 = LV_ITEM60A;
   {$ENDIF MSWindows}


const
  {$EXTERNALSYM I_IMAGENONE}
   I_IMAGENONE       = -2;
  {$EXTERNALSYM I_COLUMNSCALLBACK}
  {$IFDEF MSWindows}
   I_COLUMNSCALLBACK = UINT(-1);
  {$ENDIF MSWindows}


const
  {$EXTERNALSYM LVM_SORTITEMSEX}
   LVM_SORTITEMSEX = LVM_FIRST + 81;

type
   PFNLVCOMPARE = function(lParam1, lParam2, lParamSort: integer): integer stdcall;
   TLVCompare = PFNLVCOMPARE;

const
  {$EXTERNALSYM LVBKIF_FLAG_TILEOFFSET}
   LVBKIF_FLAG_TILEOFFSET = $00000100;
  {$EXTERNALSYM LVBKIF_TYPE_WATERMARK}
   LVBKIF_TYPE_WATERMARK  = $10000000;

const
  {$EXTERNALSYM LVM_SETSELECTEDCOLUMN}
   LVM_SETSELECTEDCOLUMN = LVM_FIRST + 140;

const
  {$EXTERNALSYM LVM_SETTILEWIDTH}
   LVM_SETTILEWIDTH = LVM_FIRST + 141;


const
  {$EXTERNALSYM LV_VIEW_ICON}
   LV_VIEW_ICON      = $0000;
  {$EXTERNALSYM LV_VIEW_DETAILS}
   LV_VIEW_DETAILS   = $0001;
  {$EXTERNALSYM LV_VIEW_SMALLICON}
   LV_VIEW_SMALLICON = $0002;
  {$EXTERNALSYM LV_VIEW_LIST}
   LV_VIEW_LIST      = $0003;
  {$EXTERNALSYM LV_VIEW_TILE}
   LV_VIEW_TILE      = $0004;
  {$EXTERNALSYM LV_VIEW_MAX}
   LV_VIEW_MAX       = $0004;

const
  {$EXTERNALSYM LVM_SETVIEW}
   LVM_SETVIEW = LVM_FIRST + 142;
  {$EXTERNALSYM LVM_GETVIEW}
   LVM_GETVIEW = LVM_FIRST + 143;

const
  {$EXTERNALSYM LVGF_NONE}
   LVGF_NONE    = $00000000;
  {$EXTERNALSYM LVGF_HEADER}
   LVGF_HEADER  = $00000001;
  {$EXTERNALSYM LVGF_FOOTER}
   LVGF_FOOTER  = $00000002;
  {$EXTERNALSYM LVGF_STATE}
   LVGF_STATE   = $00000004;
  {$EXTERNALSYM LVGF_ALIGN}
   LVGF_ALIGN   = $00000008;
  {$EXTERNALSYM LVGF_GROUPID}
   LVGF_GROUPID = $00000010;

  {$EXTERNALSYM LVGS_NORMAL}
   LVGS_NORMAL    = $00000000;
  {$EXTERNALSYM LVGS_COLLAPSED}
   LVGS_COLLAPSED = $00000001;
  {$EXTERNALSYM LVGS_HIDDEN}
   LVGS_HIDDEN    = $00000002;

   LVGS_NOHEADER    = $00000004;
   LVGS_COLLAPSIBLE = $00000008;

  {$EXTERNALSYM LVGA_HEADER_LEFT}
   LVGA_HEADER_LEFT   = $00000001;
  {$EXTERNALSYM LVGA_HEADER_CENTER}
   LVGA_HEADER_CENTER = $00000002;
  {$EXTERNALSYM LVGA_HEADER_RIGHT}
   LVGA_HEADER_RIGHT  = $00000004;  // Don't forget to validate exclusivity
  {$EXTERNALSYM LVGA_FOOTER_LEFT}
   LVGA_FOOTER_LEFT   = $00000008;
  {$EXTERNALSYM LVGA_FOOTER_CENTER}
   LVGA_FOOTER_CENTER = $00000010;
  {$EXTERNALSYM LVGA_FOOTER_RIGHT}
   LVGA_FOOTER_RIGHT  = $00000020;  // Don't forget to validate exclusivity

{$IFDEF MSWindows}

type
  {$EXTERNALSYM tagLVGROUPMETRICS}
   tagLVGROUPMETRICS = packed record
      cbSize,
      mask,
      Left,
      Top,
      Right,
      Bottom: UINT;
      crLeft,
      crTop,
      crRight,
      crBottom,
      crHeader,
      crFooter: COLORREF;
   end;
  {$EXTERNALSYM TLVGroupMetrics}
   TLVGroupMetrics = tagLVGROUPMETRICS;
  {$EXTERNALSYM PLVGroupMetrics}
   PLVGroupMetrics = ^TLVGroupMetrics;

type
  {$EXTERNALSYM PFNLVGROUPCOMPARE}
   PFNLVGROUPCOMPARE = function(lParam1, lParam2: integer; plv: pointer): integer; stdcall;
   TLVGroupCompare = PFNLVGROUPCOMPARE;


type
  {$EXTERNALSYM tagLVINSERTGROUPSORTED}
   tagLVINSERTGROUPSORTED = packed record
      pfnGroupCompare: PFNLVGROUPCOMPARE;
      pvData: pointer;
      lvGroup: TLVGroup;
   end;
  {$EXTERNALSYM TVLInsertGroupSorted}
   TVLInsertGroupSorted = tagLVINSERTGROUPSORTED;
  {$EXTERNALSYM PVLInsertGroupSorted}
   PVLInsertGroupSorted = ^TVLInsertGroupSorted;


type
  {$EXTERNALSYM tagLVTILEVIEWINFO}
   tagLVTILEVIEWINFO = packed record
      cbSize: UINT;
      dwMask,
      dwFlags: dword;
      sizeTile: SIZE;
      cLines: integer;
      rcLabelMargin: TRect;
   end;
  {$EXTERNALSYM TLVTileViewInfo}
   TLVTileViewInfo = tagLVTILEVIEWINFO;
  {$EXTERNALSYM PLVTileViewInfo}
   PLVTileViewInfo = ^TLVTileViewInfo;

  {$EXTERNALSYM tagLVTILEINFO}
   tagLVTILEINFO = packed record
      cbSize: UINT;
      iItem: integer;
      cColumns: UINT;
      puColumns: PUINT;
   end;
  {$EXTERNALSYM TLVTileInfo}
   TLVTileInfo = tagLVTILEINFO;
  {$EXTERNALSYM PLVTileInfo}
   PLVTileInfo = ^TLVTileInfo;

   {$EXTERNALSYM LVINSERTMARK}
   LVINSERTMARK = packed record
      cbSize: UINT;
      dwFlags: dword;
      iItem: integer;
      dwReserved: dword;
   end;
  {$EXTERNALSYM TLVInsertMark}
   TLVInsertMark = LVINSERTMARK;
  {$EXTERNALSYM PLVInsertMark}
   PLVInsertMark = ^TLVInsertMark;

  {$EXTERNALSYM tagLVSETINFOTIP}
   tagLVSETINFOTIP = packed record
      cbSize: UINT;
      dwFlags: dword;
      pszText: LPWSTR;
      iItem: integer;
      iSubItem: integer;
   end;
  {$EXTERNALSYM TLVSetInfoTip}
   TLVSetInfoTip = tagLVSETINFOTIP;
  {$EXTERNALSYM PLVSetInfoTip}
   PLVSetInfoTip = ^TLVSetInfoTip;

type
  {$EXTERNALSYM tagNMCUSTOMDRAWINFO}
   tagNMCUSTOMDRAWINFO = packed record
      hdr: TNMHdr;
      dwDrawStage: DWORD;
      hdc: HDC;
      rc: TRect;
      dwItemSpec: DWORD;  // this is control specific, but it's how to specify an item.  valid only with CDDS_ITEM bit set
      uItemState: UINT;
      lItemlParam: LPARAM;
   end;
   PNMCustomDraw = ^TNMCustomDraw;
   TNMCustomDraw = tagNMCUSTOMDRAWINFO;

const
  {$EXTERNALSYM NMLVCUSTOMDRAW_V3_SIZE}
   NMLVCUSTOMDRAW_V3_SIZE = sizeof(TNMCustomDraw) + (2 * sizeof(COLORREF));
  {$EXTERNALSYM NMLVCUSTOMDRAW_V4_SIZE}
   NMLVCUSTOMDRAW_V4_SIZE = NMLVCUSTOMDRAW_V3_SIZE + sizeof(integer);

type
  {$EXTERNALSYM tagNMLVCUSTOMDRAW}
   tagNMLVCUSTOMDRAW = packed record
      nmcd: TNMCustomDraw;
      clrText: COLORREF;
      clrTextBk: COLORREF;
      iSubItem: integer;

      dwItemType: dword;

      // Item custom draw
      clrFace: COLORREF;
      iIconEffect,
      iIconPhase,
      iPartId,
      iStateId: integer;

      // Group Custom Draw
      rcText: TRect;
      uAlign: UINT; // Alignment. Use LVGA_HEADER_CENTER, LVGA_HEADER_RIGHT, LVGA_HEADER_LEFT
   end;
  {$EXTERNALSYM PNMLVCustomDraw}
   PNMLVCustomDraw = ^TNMLVCustomDraw;
  {$EXTERNALSYM TNMLVCustomDraw}
   TNMLVCustomDraw = tagNMLVCUSTOMDRAW;

// dwItemType
const
  {$EXTERNALSYM LVCDI_ITEM}
   LVCDI_ITEM  = $00000000;
  {$EXTERNALSYM LVCDI_GROUP}
   LVCDI_GROUP = $00000001;

   // ListView custom draw return values
  {$EXTERNALSYM LVCDRF_NOSELECT}
   LVCDRF_NOSELECT     = $00010000;
  {$EXTERNALSYM LVCDRF_NOGROUPFRAME}
   LVCDRF_NOGROUPFRAME = $00020000;


type
  {$EXTERNALSYM tagNMLVSCROLL}
   tagNMLVSCROLL = packed record
      hdr: NMHDR;
      dx,
      dy: integer;
   end;
  {$EXTERNALSYM TNMLVScroll}
   TNMLVScroll = tagNMLVSCROLL;
  {$EXTERNALSYM PNMLVScroll}
   PNMLVScroll = ^TNMLVScroll;

const
  {$EXTERNALSYM LVN_FIRST}
   LVN_FIRST = 0 - 100;       { listview }
  {$EXTERNALSYM LVN_LAST}
   LVN_LAST  = 0 - 199;

const
  {$EXTERNALSYM LVN_BEGINSCROLL}
   LVN_BEGINSCROLL              = LVN_FIRST - 80;
  {$EXTERNALSYM LVN_ENDSCROLL}
   LVN_ENDSCROLL                = LVN_FIRST - 81;
   LVM_SETEXTENDEDLISTVIEWSTYLE = LVM_FIRST + 54;

{$EXTERNALSYM ListView_SetExtendedListViewStyleEx}
function ListView_SetExtendedListViewStyleEx(hwndLV: HWND; dwMask, dw: DWORD): dword;
{$EXTERNALSYM ListView_SortItemsEx}
function ListView_SortItemsEx(hwndLV: HWND; pfnCompare: TLVCompare; lPrm: longint): bool;
{$EXTERNALSYM ListView_SetSelectedColumn}
procedure ListView_SetSelectedColumn(wnd: HWND; iCol: integer);
{$EXTERNALSYM ListView_SetTileWidth}
function ListView_SetTileWidth(wnd: HWND; cpWidth: integer): integer;
{$EXTERNALSYM ListView_SetView}
function ListView_SetView(wnd: HWND; iView: dword): dword;
{$EXTERNALSYM ListView_GetView}
function ListView_GetView(wnd: HWND): dword;
{$EXTERNALSYM ListView_InsertGroup}
function ListView_InsertGroup(wnd: HWND; index: integer; pgrp: TLVGroup): integer;
{$EXTERNALSYM ListView_SetGroupInfo}
function ListView_SetGroupInfo(wnd: HWND; iGroupId: integer; pgrp: TLVGroup): integer;
{$EXTERNALSYM ListView_GetGroupInfo}
function ListView_GetGroupInfo(wnd: HWND; iGroupId: integer; var pgrp: TLVGroup): integer;
{$EXTERNALSYM ListView_RemoveGroup}
function ListView_RemoveGroup(wnd: HWND; iGroupId: integer): integer;
{$EXTERNALSYM ListView_MoveGroup}
procedure ListView_MoveGroup(wnd: HWND; iGroupId, toIndex: integer);
{$EXTERNALSYM ListView_MoveItemToGroup}
procedure ListView_MoveItemToGroup(wnd: HWND; idItemFrom, idGroupTo: integer);
{$EXTERNALSYM ListView_SetGroupMetrics}
procedure ListView_SetGroupMetrics(wnd: HWND; pGroupMetrics: TLVGroupMetrics);
{$EXTERNALSYM ListView_GetGroupMetrics}
procedure ListView_GetGroupMetrics(wnd: HWND; var pGroupMetrics: TLVGroupMetrics);
{$EXTERNALSYM ListView_EnableGroupView}
function ListView_EnableGroupView(wnd: HWND; fEnable: bool): integer;
{$EXTERNALSYM ListView_SortGroups}
function ListView_SortGroups(wnd: HWND; fnGroupCompare: TLVGroupCompare; plv: pointer): integer;
{$EXTERNALSYM ListView_SetTileViewInfo}
function ListView_SetTileViewInfo(AListViewHandle: HWND; ptvi: TLVTileViewInfo): bool;
{$EXTERNALSYM ListView_GetTileViewInfo}
procedure ListView_GetTileViewInfo(AListViewHandle: HWND; var ptvi: TLVTileViewInfo);
{$EXTERNALSYM ListView_SetTileInfo}
function ListView_SetTileInfo(AListViewHandle: HWND; pti: TLVTileInfo): bool;
{$EXTERNALSYM ListView_GetTileInfo}
procedure ListView_GetTileInfo(AListViewHandle: HWND; var pti: TLVTileInfo);
procedure ListView_SetGroupSubItemCount(AListViewHandle: HWND; AGroupID: integer; AItemCount: integer);
{$EXTERNALSYM ListView_GetInsertMark}
function ListView_GetInsertMark(wnd: HWND; var lvim: TLVInsertMark): bool;
{$EXTERNALSYM ListView_InsertMarkHitTest}
function ListView_InsertMarkHitTest(wnd: HWND; point: TPoint; lvim: TLVInsertMark): integer;
{$EXTERNALSYM ListView_GetInsertMarkRect}
function ListView_GetInsertMarkRect(wnd: HWND; var rc: TRect): integer;
{$EXTERNALSYM ListView_SetInsertMarkColor}
function ListView_SetInsertMarkColor(wnd: HWND; color: COLORREF): COLORREF;
{$EXTERNALSYM ListView_GetInsertMarkColor}
function ListView_GetInsertMarkColor(wnd: HWND): COLORREF;
{$EXTERNALSYM ListView_InsertGroupSorted}
procedure ListView_InsertGroupSorted(wnd: HWND; structInsert: TVLInsertGroupSorted);
{$EXTERNALSYM ListView_RemoveAllGroups}
procedure ListView_RemoveAllGroups(wnd: HWND);
{$EXTERNALSYM ListView_HasGroup}
function ListView_HasGroup(wnd: HWND; dwGroupId: dword): bool;
{$EXTERNALSYM ListView_SetInsertMark}
function ListView_SetInsertMark(wnd: HWND; lvim: TLVInsertMark): bool;
{$EXTERNALSYM ListView_SetInfoTip}
function ListView_SetInfoTip(hwndLV: HWND; plvInfoTip: TLVSetInfoTip): bool;
{$EXTERNALSYM ListView_GetSelectedColumn}
function ListView_GetSelectedColumn(wnd: HWND): UINT;
function ListView_SetColumnWidth(hwnd: HWnd; iCol: integer; cx: integer): Bool;
{$EXTERNALSYM ListView_IsGroupViewEnabled}
function ListView_IsGroupViewEnabled(wnd: HWND): bool;
{$EXTERNALSYM ListView_SetOutlineColor}
function ListView_SetOutlineColor(wnd: HWND; color: COLORREF): COLORREF;
{$EXTERNALSYM ListView_CancelEditLabel}
procedure ListView_CancelEditLabel(wnd: HWND);
{$EXTERNALSYM ListView_MapIndexToID}
function ListView_MapIndexToID(wnd: HWND; index: uint): UINT;
{$EXTERNALSYM ListView_MapIDToIndex}
function ListView_MapIDToIndex(wnd: HWND; id: uint): UINT;
{$EXTERNALSYM ListView_GetOutlineColor}
function ListView_GetOutlineColor(wnd: HWND): COLORREF;
procedure InitCommonControls; stdcall;
{$ENDIF MSWindows}


implementation

{$IFDEF MSWindows}
const
   cctrl = comctl32; { From Windows.pas }

procedure InitCommonControls; stdcall; external cctrl Name 'InitCommonControls';

function ListView_SetExtendedListViewStyleEx(hwndLV: HWND; dwMask, dw: DWORD): dword;
begin
   Result := dword(SendMessage(hwndLV, LVM_SETEXTENDEDLISTVIEWSTYLE, dwMask, dw));
end;

function ListView_SortItemsEx(hwndLV: HWND; pfnCompare: TLVCompare; lPrm: longint): bool;
begin
   Result := bool(SendMessage(hwndLV, LVM_SORTITEMSEX, WPARAM(lPrm), LPARAM(@pfnCompare)));
end;

procedure ListView_SetSelectedColumn(wnd: HWND; iCol: integer);
begin
   SendMessage(wnd, LVM_SETSELECTEDCOLUMN, WPARAM(iCol), 0);
end;

function ListView_SetTileWidth(wnd: HWND; cpWidth: integer): integer;
begin
   Result := integer(SendMessage(wnd, LVM_SETTILEWIDTH, WPARAM(cpWidth), 0));
end;

function ListView_SetView(wnd: HWND; iView: dword): dword;
begin
   Result := dword(SendMessage(wnd, LVM_SETVIEW, WPARAM(iView), 0));
end;

function ListView_GetView(wnd: HWND): dword;
begin
   Result := dword(SendMessage(wnd, LVM_GETVIEW, 0, 0));
end;

function ListView_InsertGroup(wnd: HWND; index: integer; pgrp: TLVGroup): integer;
begin
   Result := integer(SendMessage(wnd, LVM_INSERTGROUP, index, LPARAM(@pgrp)));
end;

function ListView_SetGroupInfo(wnd: HWND; iGroupId: integer; pgrp: TLVGroup): integer;
begin
   Result := integer(SendMessage(wnd, LVM_SETGROUPINFO, iGroupId, LPARAM(@pgrp)));
end;

function ListView_GetGroupInfo(wnd: HWND; iGroupId: integer; var pgrp: TLVGroup): integer;
begin
   Result := integer(SendMessage(wnd, LVM_GETGROUPINFO, WPARAM(iGroupId), LPARAM(@pgrp)));
end;

function ListView_RemoveGroup(wnd: HWND; iGroupId: integer): integer;
begin
   Result := integer(SendMessage(wnd, LVM_REMOVEGROUP, WPARAM(iGroupId), 0));
end;

procedure ListView_MoveGroup(wnd: HWND; iGroupId, toIndex: integer);
begin
   SendMessage(wnd, LVM_MOVEGROUP, WPARAM(iGroupId), LPARAM(toIndex));
end;

procedure ListView_MoveItemToGroup(wnd: HWND; idItemFrom, idGroupTo: integer);
begin
   SendMessage(wnd, LVM_MOVEITEMTOGROUP, WPARAM(idItemFrom), LPARAM(idGroupTo));
end;

procedure ListView_SetGroupMetrics(wnd: HWND; pGroupMetrics: TLVGroupMetrics);
begin
   SendMessage(wnd, LVM_SETGROUPMETRICS, 0, LPARAM(@pGroupMetrics));
end;

procedure ListView_GetGroupMetrics(wnd: HWND; var pGroupMetrics: TLVGroupMetrics);
begin
   SendMessage(wnd, LVM_GETGROUPMETRICS, 0, LPARAM(@pGroupMetrics));
end;

function ListView_EnableGroupView(wnd: HWND; fEnable: bool): integer;
begin
   Result := integer(SendMessage(wnd, LVM_ENABLEGROUPVIEW, WPARAM(fEnable), 0));
end;

function ListView_SortGroups(wnd: HWND; fnGroupCompare: TLVGroupCompare; plv: pointer): integer;
begin
   Result := integer(SendMessage(wnd, LVM_SORTGROUPS, WPARAM(@fnGroupCompare), LPARAM(plv)));
end;

procedure ListView_InsertGroupSorted(wnd: HWND; structInsert: TVLInsertGroupSorted);
begin
   SendMessage(wnd, LVM_INSERTGROUPSORTED, WPARAM(@structInsert), 0);
end;

procedure ListView_RemoveAllGroups(wnd: HWND);
begin
   SendMessage(wnd, LVM_REMOVEALLGROUPS, 0, 0);
end;

function ListView_HasGroup(wnd: HWND; dwGroupId: dword): bool;
begin
   Result := bool(SendMessage(wnd, LVM_HASGROUP, WPARAM(dwGroupId), 0));
end;

function ListView_SetTileViewInfo(AListViewHandle: HWND; ptvi: TLVTileViewInfo): bool;
begin
   Result := bool(SendMessage(AListViewHandle, LVM_SETTILEVIEWINFO, 0, LPARAM(@ptvi)));
end;

procedure ListView_GetTileViewInfo(AListViewHandle: HWND; var ptvi: TLVTileViewInfo);
begin
   SendMessage(AListViewHandle, LVM_GETTILEVIEWINFO, 0, LPARAM(@ptvi));
end;

function ListView_SetTileInfo(AListViewHandle: HWND; pti: TLVTileInfo): bool;
begin
   Result := bool(SendMessage(AListViewHandle, LVM_SETTILEINFO, 0, LPARAM(@pti)));
end;

procedure ListView_GetTileInfo(AListViewHandle: HWND; var pti: TLVTileInfo);
begin
   SendMessage(AListViewHandle, LVM_GETTILEINFO, 0, LPARAM(@pti));
end;

procedure ListView_SetGroupSubItemCount(AListViewHandle: HWND; AGroupID: integer; AItemCount: integer);
begin
   SendMessage(AListViewHandle, LVM_SETGROUPSUBSETCOUNT, AGroupID, AItemCount);
end;

function ListView_SetInsertMark(wnd: HWND; lvim: TLVInsertMark): bool;
begin
   Result := bool(SendMessage(wnd, LVM_SETINSERTMARK, 0, LPARAM(@lvim)));
end;

function ListView_GetInsertMark(wnd: HWND; var lvim: TLVInsertMark): bool;
begin
   Result := bool(SendMessage(wnd, LVM_GETINSERTMARK, 0, LPARAM(@lvim)));
end;

function ListView_InsertMarkHitTest(wnd: HWND; point: TPoint; lvim: TLVInsertMark): integer;
begin
   Result := integer(SendMessage(wnd, LVM_INSERTMARKHITTEST, WPARAM(@point), LPARAM(@lvim)));
end;

function ListView_GetInsertMarkRect(wnd: HWND; var rc: TRect): integer;
begin
   Result := integer(SendMessage(wnd, LVM_GETINSERTMARKRECT, 0, LPARAM(@rc)));
end;

function ListView_SetInsertMarkColor(wnd: HWND; color: COLORREF): COLORREF;
begin
   Result := COLORREF(SendMessage(wnd, LVM_SETINSERTMARKCOLOR, 0, color));
end;

function ListView_GetInsertMarkColor(wnd: HWND): COLORREF;
begin
   Result := COLORREF(SendMessage(wnd, LVM_GETINSERTMARKCOLOR, 0, 0));
end;

function ListView_SetInfoTip(hwndLV: HWND; plvInfoTip: TLVSetInfoTip): bool;
begin
   Result := bool(SendMessage(hwndLV, LVM_SETINFOTIP, 0, LPARAM(@plvInfoTip)));
end;

function ListView_GetSelectedColumn(wnd: HWND): UINT;
begin
   Result := UINT(SendMessage(wnd, LVM_GETSELECTEDCOLUMN, 0, 0));
end;

function ListView_IsGroupViewEnabled(wnd: HWND): bool;
begin
   Result := bool(SendMessage(wnd, LVM_ISGROUPVIEWENABLED, 0, 0));
end;

function ListView_GetOutlineColor(wnd: HWND): COLORREF;
begin
   Result := COLORREF(SendMessage(wnd, LVM_GETOUTLINECOLOR, 0, 0));
end;

function ListView_SetOutlineColor(wnd: HWND; color: COLORREF): COLORREF;
begin
   Result := COLORREF(SendMessage(wnd, LVM_SETOUTLINECOLOR, 0, color));
end;

procedure ListView_CancelEditLabel(wnd: HWND);
begin
   SendMessage(wnd, LVM_CANCELEDITLABEL, 0, 0);
end;

function ListView_MapIndexToID(wnd: HWND; index: uint): UINT;
begin
   Result := UINT(SendMessage(wnd, LVM_MAPINDEXTOID, index, 0));
end;

function ListView_MapIDToIndex(wnd: HWND; id: uint): UINT;
begin
   Result := UINT(SendMessage(wnd, LVM_MAPIDTOINDEX, id, 0));
end;

function ListView_SetColumnWidth(hwnd: HWnd; iCol: integer; cx: integer): Bool;
begin
   Result := Bool(SendMessage(hwnd, LVM_SETCOLUMNWIDTH, iCol, MakeLong((cx), 0)));
end;

{$ENDIF MSWindows}

end.
