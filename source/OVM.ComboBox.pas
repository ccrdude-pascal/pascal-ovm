{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(ComboBox enhancement.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-08-04  pk   10m  Created new support for TComboBox
// *****************************************************************************
   )
}
unit OVM.ComboBox;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$modeswitch prefixedattributes}
//{$modeswitch multiscopehelpers}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   StdCtrls,
   Generics.Collections,
   TypInfo,
   DebugLog,
   OVM.Basics,
   OVM.ComboBox.Attributes;

type

   { TObjectComboBoxClassHelper }

   TObjectComboBoxClassHelper = class helper for TComboBox
   public
      procedure DisplayItems<T: class>(TheItems: TEnumerable<T>); overload;
      function GetSelectedKey(ADefault: string): string;
      function GetSelectedItem<T: class>: T;
      procedure SetSelectedItem<T: class>(AnItem: T);
   end;

implementation

{ TObjectComboBoxClassHelper }

procedure TObjectComboBoxClassHelper.DisplayItems<T>(TheItems: TEnumerable<T>);
var
   item: T;
   sText: string;
   iCount: integer;
   iProperty: integer;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   a: AComboBoxDisplayText;
   selectedItem: T;
begin
   selectedItem := Self.GetSelectedItem<T>();
   Self.Items.BeginUpdate;
   try
      Self.Items.Clear;
      for item in TheItems do begin
         sText := item.ClassName;
         iCount := GetPropList(item.ClassInfo, tkAny, @objectPropertyList, False);
         for iProperty := 0 to Pred(iCount) do begin
            try
               objectPropertyInfo := objectPropertyList[iProperty];
               a := AComboBoxDisplayText(GetPropAttributeFromTypInfo(objectPropertyInfo, AComboBoxDisplayText));
               if Assigned(a) then begin
                  sText := ObjectPropertyToDisplayText(item, objectPropertyInfo);
               end;
            except
               on E: Exception do begin
                  DebugLogger.LogException(E, Format('TObjectComboBoxClassHelper.DisplayItems(x, %s)', [item.ClassName]));
               end;
            end;
         end;
         Self.Items.AddObject(sText, item);
      end;
   finally
      Self.Items.EndUpdate;
      Self.SetSelectedItem<T>(selectedItem);
   end;
end;

function TObjectComboBoxClassHelper.GetSelectedKey(ADefault: string): string;
var
   iCount: integer;
   iProperty: integer;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   a: AComboBoxUniqueIDField;
   item: TObject;
begin
   Result := ADefault;
   if Self.ItemIndex < 0 then begin
      Exit;
   end;
   item := TObject(Self.Items.Objects[Self.ItemIndex]);
   if not Assigned(item) then begin
      Exit;
   end;
   iCount := GetPropList(item.ClassInfo, tkAny, @objectPropertyList, False);
   for iProperty := 0 to Pred(iCount) do begin
      try
         objectPropertyInfo := objectPropertyList[iProperty];
         a := AComboBoxUniqueIDField(GetPropAttributeFromTypInfo(objectPropertyInfo, AComboBoxUniqueIDField));
         if Assigned(a) then begin
            Result := ObjectPropertyToDisplayText(item, objectPropertyInfo);
         end;
      except
         on E: Exception do begin
            DebugLogger.LogException(E, Format('TObjectComboBoxClassHelper.GetSelectedKey(%s, %s)', [ADefault, item.ClassName]));
         end;
      end;
   end;
end;

function TObjectComboBoxClassHelper.GetSelectedItem<T>: T;
begin
   Result := nil;
   if Self.ItemIndex < 0 then begin
      Exit;
   end;
   Result := T(Self.Items.Objects[Self.ItemIndex]);
end;

procedure TObjectComboBoxClassHelper.SetSelectedItem<T>(AnItem: T);
var
   i: integer;
   item: T;
begin
   if not Assigned(AnItem) then begin
      Exit;
   end;
   for i := 0 to Pred(Self.Items.Count) do begin
      item := T(Self.Items.Objects[i]);
      if item = AnItem then begin
         Self.ItemIndex := i;
         Exit;
      end;
   end;
end;

end.
