{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Class and property attributes to manage how they are displayed in TListView.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-25  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit OVM.ListView.Attributes;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$modeswitch prefixedattributes}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils;

type
   { AListViewGrouping }

   AListViewGrouping = class(TCustomAttribute)
   private
      FGroupProperty: string;
      FGroupView: boolean;
   public
      constructor Create(AGroupView: boolean; AGroupProperty: string = '');
      property GroupView: boolean read FGroupView;
      property GroupProperty: string read FGroupProperty;
   end;

   { ATableGrouping }

   ATableGrouping = class(TCustomAttribute)
   private
      FShowHeaders: boolean;
   public
      constructor Create(AShowHeaders: boolean);
      property ShowHeaders: boolean read FShowHeaders;
   end;

   { AListViewColumnDetails }

   AListViewColumnDetails = class(TCustomAttribute)
   private
      FAlignment: TAlignment;
      FColumnName: string;
      FFormatString: string;
   public
      constructor Create(AColumnName: string; AnAlignment: TAlignment = taLeftJustify; AFormatString: string = '');
      property ColumnName: string read FColumnName;
      property FormatString: string read FFormatString;
      property Alignment: TAlignment read FAlignment;
   end;

   { AListViewSkipField }

   AListViewSkipField = class(TCustomAttribute)
   public
      constructor Create();
   end;

   { AListViewTileColumnField }

   AListViewTileColumnField = class(TCustomAttribute)
   public
      constructor Create();
   end;

   { AListViewGroupColumn }

   AListViewGroupColumn = class(TCustomAttribute)
   public
      constructor Create();
   end;

   { APropertyListViewGroup }

   APropertyListViewGroup = class(TCustomAttribute)
   private
      FGroupName: string;
   public
      constructor Create(AGroupName: string);
      property GroupName: string read FGroupName;
   end;

   { APropertyListViewKeyName }

   APropertyListViewKeyName = class(TCustomAttribute)
   private
      FKeyName: string;
   public
      constructor Create(AKeyName: string);
      property KeyName: string read FKeyName;
   end;

   { APropertyListViewBooleanValueItemIndexes }

   APropertyListViewBooleanItemIndexes = class(TCustomAttribute)
   private
      FImageIndexFalse: integer;
      FImageIndexTrue: integer;
   public
      constructor Create(AImageIndexTrue, AImageIndexFalse: integer);
      property ImageIndexTrue: integer read FImageIndexTrue;
      property ImageIndexFalse: integer read FImageIndexFalse;
   end;

   APropertyListViewBooleanValueItemIndexes = class(APropertyListViewBooleanItemIndexes)

   end;

   APropertyListViewBooleanKeyItemIndexes = class(APropertyListViewBooleanItemIndexes)

   end;

   { APropertyListViewSkipIfEmpty }

   APropertyListViewSkipIfEmpty = class(TCustomAttribute)
   public
      constructor Create();
   end;

   { APropertyListViewSkipIfZero }

   APropertyListViewSkipIfZero = class(TCustomAttribute)
   public
      constructor Create();
   end;

implementation

uses
   Firefly.i18n;

   { AListViewGroupColumn }

constructor AListViewGroupColumn.Create();
begin

end;

{ APropertyListViewGroup }

constructor APropertyListViewGroup.Create(AGroupName: string);
begin
   Self.FGroupName := AGroupName;
end;

{ APropertyListViewKeyName }

constructor APropertyListViewKeyName.Create(AKeyName: string);
begin
   Self.FKeyName := AKeyName;
end;

{ APropertyListViewBooleanItemIndexes }

constructor APropertyListViewBooleanItemIndexes.Create(AImageIndexTrue, AImageIndexFalse: integer);
begin
   Self.FImageIndexTrue := AImageIndexTrue;
   Self.FImageIndexFalse := AImageIndexFalse;
end;

{ APropertyListViewSkipIfEmpty }

constructor APropertyListViewSkipIfEmpty.Create();
begin

end;

{ APropertyListViewSkipIfZero }

constructor APropertyListViewSkipIfZero.Create();
begin

end;

{ AListViewGrouping }

constructor AListViewGrouping.Create(AGroupView: boolean; AGroupProperty: string);
begin
   Self.FGroupView := AGroupView;
   Self.FGroupProperty := AGroupProperty;
end;

{ ATableGrouping }

constructor ATableGrouping.Create(AShowHeaders: boolean);
begin
   Self.FShowHeaders := AShowHeaders;
end;

{ AListViewColumnDetails }

constructor AListViewColumnDetails.Create(AColumnName: string; AnAlignment: TAlignment; AFormatString: string);
begin
   Self.FColumnName := AColumnName;
   Self.FAlignment := AnAlignment;
   Self.FFormatString := AFormatString;
end;

{ AListViewSkipField }

constructor AListViewSkipField.Create();
begin

end;

{ AListViewTileColumnField }

constructor AListViewTileColumnField.Create();
begin

end;

end.
