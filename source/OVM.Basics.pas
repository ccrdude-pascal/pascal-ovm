unit OVM.Basics;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   TypInfo;

type
   TCustomAttributeClass = class of TCustomAttribute;

type
   TFieldNameToPublicOption = (fntpoStripInitialLowerCase, fntpoCapitalizeFirstLetter, fntpoReplaceUnderscores, fntpoInsertSpaces);
   TFieldNameToPublicOptions = set of TFieldNameToPublicOption;

function OVMFieldNameToPublic(TheFieldName: string; TheConversionOptions: TFieldNameToPublicOptions = [fntpoStripInitialLowerCase, fntpoCapitalizeFirstLetter,
   fntpoReplaceUnderscores, fntpoInsertSpaces]): string;
function GetDisplayNameForProperty(ThePropertyInfo: PPropInfo; out TheAlignment: TAlignment): string;
function ObjectPropertyToDisplayText(TheItem: TObject; ThePropertyInfo: PPropInfo): string; overload;
function ObjectPropertyToDisplayText(TheItem: TObject; ThePropertyName: string): string; overload;
function GetTypeAttributeFromTypInfo(TheTypeInfo: PTypeInfo; ADesiredClass: TCustomAttributeClass): TCustomAttribute;
function GetPropAttributeFromTypInfo(ThePropertyInfo: PPropInfo; ADesiredClass: TCustomAttributeClass): TCustomAttribute;

implementation

uses
   OVM.Attributes,
   OVM.ListView.Attributes;

function OVMFieldNameToPublic(TheFieldName: string; TheConversionOptions: TFieldNameToPublicOptions): string;
var
   iLetter: integer;
   bLetterIsUpCase: boolean;
   bPreviousIsWhitespace: boolean;
   bPreviousIsLoCase: boolean;
begin
   while (fntpoStripInitialLowerCase in TheConversionOptions) and (Length(TheFieldName) > 0) and (TheFieldName[1] = LowerCase(TheFieldName[1])) and (not (TheFieldName[1] in ['0'..'9'])) do begin
      TheFieldName := Copy(TheFieldName, 2, Length(TheFieldName) - 1);
   end;
   if (fntpoCapitalizeFirstLetter in TheConversionOptions) and (Length(TheFieldName) > 0) then begin
      TheFieldName[1] := UpCase(TheFieldName[1]);
   end;
   if (fntpoReplaceUnderscores in TheConversionOptions) and (Length(TheFieldName) > 0) then begin
      TheFieldName := StringReplace(TheFieldName, '_', ' ', [rfReplaceAll]);
   end;
   if (fntpoInsertSpaces in TheConversionOptions) and (Length(TheFieldName) > 0) then begin
      for iLetter := Length(TheFieldName) downto 2 do begin
         bLetterIsUpCase := (TheFieldName[iLetter] = UpCase(TheFieldName[iLetter])) and (not (TheFieldName[iLetter] in ['0'..'9']));
         bPreviousIsWhitespace := (TheFieldName[iLetter - 1] = ' ');
         bPreviousIsLoCase := (TheFieldName[iLetter - 1] = LowerCase(TheFieldName[iLetter - 1]));
         if (bLetterIsUpCase) and (not bPreviousIsWhitespace) and (bPreviousIsLoCase) then begin
            Insert(' ', TheFieldName, iLetter);
         end;
      end;
   end;
   Result := TheFieldName;
end;

function GetDisplayNameForProperty(ThePropertyInfo: PPropInfo; out TheAlignment: TAlignment): string;
var
   aColumnDetails: AListViewColumnDetails;
begin
   aColumnDetails := AListViewColumnDetails(GetPropAttributeFromTypInfo(ThePropertyInfo, AListViewColumnDetails));
   if Assigned(aColumnDetails) then begin
      Result := aColumnDetails.ColumnName;
      TheAlignment := aColumnDetails.Alignment;
   end else begin
      Result := OVMFieldNameToPublic(ThePropertyInfo^.Name);
      TheAlignment := taLeftJustify;
   end;
end;

function ObjectPropertyToDisplayText(TheItem: TObject; ThePropertyInfo: PPropInfo): string;
var
   aColumnDetails: AListViewColumnDetails;
   aBooleanYesNo: AOVMBooleanYesNo;
   s: string;
   f: double;
   i64Value: int64;
   ui64Value: uint64;
begin
   aColumnDetails := AListViewColumnDetails(GetPropAttributeFromTypInfo(ThePropertyInfo, AListViewColumnDetails));
   Str(ThePropertyInfo^.PropType^.Kind, s);
   try
      case ThePropertyInfo^.PropType^.Kind of
         tkEnumeration: begin
            Result := OVMFieldNameToPublic(GetEnumProp(TheItem, ThePropertyInfo));
            if Assigned(aColumnDetails) and (Length(aColumnDetails.FormatString) > 0) then begin
               Result := Format(aColumnDetails.FormatString, [Result]);
            end;
         end;
         tkSet: begin
            Result := GetSetProp(TheItem, ThePropertyInfo, false);
         end;
         tkAString: begin
            Result := GetStrProp(TheItem, ThePropertyInfo);
            if Assigned(aColumnDetails) and (Length(aColumnDetails.FormatString) > 0) then begin
               Result := Format(aColumnDetails.FormatString, [Result]);
            end;
         end;
         tkWString: begin
            try
               Result := UTF8Encode(GetWideStrProp(TheItem, ThePropertyInfo));
               if Assigned(aColumnDetails) and (Length(aColumnDetails.FormatString) > 0) then begin
                  Result := Format(aColumnDetails.FormatString, [Result]);
               end;
            except
               Result := '?';
            end;
         end;
         tkUString: begin
            Result := UTF8Encode(GetUnicodeStrProp(TheItem, ThePropertyInfo));
            if Assigned(aColumnDetails) and (Length(aColumnDetails.FormatString) > 0) then begin
               Result := Format(aColumnDetails.FormatString, [Result]);
            end;
         end;
         tkInteger, tkInt64: begin
            i64Value := GetInt64Prop(TheItem, ThePropertyInfo);
            if Assigned(aColumnDetails) and (Length(aColumnDetails.FormatString) > 0) then begin
               Result := Format(aColumnDetails.FormatString, [i64Value]);
            end else begin
               Result := IntToStr(i64Value);
            end;
         end;
         tkQWord: begin
            ui64Value := GetInt64Prop(TheItem, ThePropertyInfo);
            if Assigned(aColumnDetails) and (Length(aColumnDetails.FormatString) > 0) then begin
               Result := Format(aColumnDetails.FormatString, [ui64Value]);
            end else begin
               Result := IntToStr(ui64Value);
            end;
         end;
         tkBool: begin
            aBooleanYesNo := AOVMBooleanYesNo(GetPropAttributeFromTypInfo(ThePropertyInfo, AOVMBooleanYesNo));
            if Assigned(aBooleanYesNo) then begin
               Result := BoolToStr(GetInt64Prop(TheItem, ThePropertyInfo) = 1, aBooleanYesNo.TextTrue, aBooleanYesNo.TextFalse);
            end else begin
               Result := BoolToStr(GetInt64Prop(TheItem, ThePropertyInfo) = 1, True);
            end;
         end;
         tkFloat: begin
            f := GetFloatProp(TheItem, ThePropertyInfo);
            if Assigned(aColumnDetails) and (Length(aColumnDetails.FormatString) > 0) then begin
               Result := Format(aColumnDetails.FormatString, [f]);
            end else begin
               if (ThePropertyInfo^.PropType^.Name = 'TDateTime') then begin
                  if (f <> 0) and (f < 1) then begin
                     Result := FormatDateTime('hh:nn:ss.zzz', TDateTime(double(f)));
                  end else if (f = 0) then begin
                     Result := 'n/a';
                  end else begin
                     Result := FormatDateTime('yyyy-mm-dd hh:nn', TDateTime(double(f)));
                  end;
               end else if (ThePropertyInfo^.PropType^.Name = 'TDate') then begin
                  Result := FormatDateTime('yyyy-mm-dd', TDateTime(double(f)));
               end else if (ThePropertyInfo^.PropType^.Name = 'TDuration') then begin
                  Result := FormatDateTime('hh:nn:ss.zzz', TDateTime(double(f)));
               end else begin
                  Result := FloatToStr(f);
               end;
            end;
         end;
         else
         begin
            Result := s;
         end;
      end;
   except
      on E: Exception do begin
         //DebugLogger.LogException(E, Format('TObjectListViewClassHelper.PropertyToDisplayText(%s, %s)', [TheItem.ClassName, ThePropertyInfo^.PropType^.Name]));
         Result := '?';
      end;
   end;
end;

function ObjectPropertyToDisplayText(TheItem: TObject; ThePropertyName: string): string;
var
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   iProperty: integer;
   iPropertyCount: integer;
begin
   Result := '';
   // iterate properties
   iPropertyCount := GetPropList(TheItem.ClassInfo, tkAny, @objectPropertyList, False);
   for iProperty := 0 to Pred(iPropertyCount) do begin
      try
         objectPropertyInfo := objectPropertyList[iProperty];
         if SameText(ThePropertyName, objectPropertyInfo^.Name) then begin
            Result := ObjectPropertyToDisplayText(TheItem, objectPropertyInfo);
         end;
      except
         on E: Exception do begin
            //DebugLogger.LogException(E, Format('TObjectListViewClassHelper.UpdateListItemForObject(x, %s)', [AnItem.ClassName]));
         end;
      end;
   end;
   // if matching, get propinfo and call above
end;

function GetTypeAttributeFromTypInfo(TheTypeInfo: PTypeInfo; ADesiredClass: TCustomAttributeClass): TCustomAttribute;
var
   iAttribute: integer;
   attributeEntry: TAttributeEntry;
   attribute: TCustomAttribute;
   pat: PAttributeTable;
begin
   Result := nil;
   pat := GetAttributeTable(TheTypeInfo);
   if Assigned(pat) then begin
      for iAttribute := 0 to Pred(pat^.AttributeCount) do begin
         attributeEntry := pat^.AttributesList[iAttribute];
         attribute := attributeEntry.AttrProc;
         if attribute is ADesiredClass then begin
            Result := attribute;
            Exit;
         end;
      end;
   end;
end;

function GetPropAttributeFromTypInfo(ThePropertyInfo: PPropInfo; ADesiredClass: TCustomAttributeClass): TCustomAttribute;
var
   iAttribute: integer;
   attributeEntry: TAttributeEntry;
   attribute: TCustomAttribute;
begin
   Result := nil;
   if Assigned(ThePropertyInfo^.AttributeTable) then begin
      for iAttribute := 0 to Pred(ThePropertyInfo^.AttributeTable.AttributeCount) do begin
         attributeEntry := ThePropertyInfo^.AttributeTable.AttributesList[iAttribute];
         attribute := attributeEntry.AttrProc;
         if attribute is ADesiredClass then begin
            Result := attribute;
            Exit;
         end;
      end;
   end;
end;

end.
