unit OVM.ListView.JSON;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   ComCtrls,
   fpjson,
   OVM.ListView;

type

   { TJSONListViewClassHelper }

   TJSONListViewClassHelper = class helper(TObjectListViewClassHelper) for TListView
   public
      function UpdateListItemForJSONObject(AListItem: TListItem; AnItem: TJSONObject): integer;
   end;


implementation

uses
   DebugLog;

{ TJSONListViewClassHelper }

function TJSONListViewClassHelper.UpdateListItemForJSONObject(AListItem: TListItem; AnItem: TJSONObject): integer;
var
   iCurrent: integer;
   sName: string;
   sData: string;
   lc: TListColumn;
   dField: TJSONData;
   dText: TJSONString;
begin
   AListItem.Data := AnItem;
   for iCurrent := 0 to Pred(AnItem.Count) do begin
      try
         sName := AnItem.Names[iCurrent];
         lc := Self.FindOrCreateColumn(sName);
         dField := AnItem.Elements[sName];
         if (dField.JSONType = jtObject) then begin
            if (dField as TJSONObject).Find('iso8601', dText) then begin
               sData := dText.AsString;
            end;
         end else begin
            sData := AnItem.Strings[sName];
         end;
         if (lc.Index = 0) then begin
            AListItem.Caption := sData;
         end else begin
            while AListItem.SubItems.Count < lc.Index do begin
               AListItem.SubItems.Add('');
            end;
            AListItem.SubItems[lc.Index - 1] := sData;
         end;
      except
         on E: Exception do begin
            DebugLogger.LogException(E, Format('TJSONListViewClassHelper.UpdateListItemForJSONObject(x, %s)', [AnItem.ClassName]));
         end;
      end;
   end;
end;

end.
