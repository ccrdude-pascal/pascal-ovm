{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(TVirtualStringTree enhancement.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-10-04  pk   10m  Created new support for TTreeView
// *****************************************************************************
   )
}
unit OVM.VirtualTreeView;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$modeswitch prefixedattributes}
//{$modeswitch multiscopehelpers}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   StdCtrls,
   Controls,
   VirtualTrees,
   Generics.Collections,
   TypInfo,
   Rtti,
   Graphics,
   DebugLog,
   OVM.Basics,
   OVM.Attributes,
   OVM.ListView.Attributes,
   OVM.TreeView.Attributes;

type

   { TObjectVirtualStringTreeData }

   TObjectVirtualStringTreeData = record
      DataItem: TObject;
      FirstText: string;
      ImageIndex: integer;
      ItemCount: integer;
      OnExecute: TNotifyEvent;
      ItemData: array of string;
      IsButton: array of boolean;
      Controls: array of TControl;
   end;
   PObjectVirtualStringTreeData = ^TObjectVirtualStringTreeData;

   { TObjectVirtualStringTreeClassHelper }

   TObjectVirtualStringTreeClassHelper = class helper for TVirtualStringTree
   protected
      procedure ObjectGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
      procedure ObjectGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: boolean; var ImageIndex: integer);
      procedure ObjectDblClick(Sender: TObject);
      procedure ObjectAfterPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas);
      function UpdateTreeDataForObject(AData: PObjectVirtualStringTreeData; AnItem: TObject): integer;
      function AddColumn(AName: string): TVirtualTreeColumn;
      function FindOrCreateColumn(AName: string): TVirtualTreeColumn;
      procedure SetNodeControlVisible(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex = NoColumn);
      procedure SetNodesControlVisibleProc(Sender: TBaseVirtualTree; Node: PVirtualNode; Data: Pointer; var Abort: boolean);
      function IsNodeVisibleInClientRect(Node: PVirtualNode; Column: TColumnIndex = NoColumn): boolean;
   public
      function GetItemIndexKey(TheSelectedNode: PVirtualNode; AnIdentifierClass: TCustomAttributeClass; ADefault: string = ''): string;
      procedure DisplayItems<T: class>(TheItems: TEnumerable<T>); overload;
      function FindParentUsingKey<T: class>(AParentKey: string): PVirtualNode;
      function FindParentUsingObject<T: class>(AParent: T): PVirtualNode;
      function GetSelectedKey(ADefault: string = ''): string;
      function GetSelectedParentKey(ADefault: string = ''): string;
      function GetObjectFromNode(ANode: PVirtualNode): TObject;
      function GetSelectedItem<T: class>: T;
      procedure SetSelectedItem<T: class>(AnItem: T);
   end;


implementation

uses
   Types;

{ TObjectVirtualStringTreeClassHelper }

procedure TObjectVirtualStringTreeClassHelper.ObjectGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
   p: PObjectVirtualStringTreeData;
begin
   p := Self.GetNodeData(Node);
   CellText := p^.FirstText;
   if (Column > 0) and (Column < p^.ItemCount) then begin
      if (Assigned(p^.Controls[Column])) then begin
         CellText := '';
      end else begin
         CellText := p^.ItemData[Column];
      end;
      (*
      // debug code to check IsButton flag only
      if p^.IsButton[Column] then begin
         CellText := '[ ' + CellText + ' ]';
      end;
      *)
   end;
end;

procedure TObjectVirtualStringTreeClassHelper.ObjectGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
   var Ghosted: boolean; var ImageIndex: integer);
var
   p: PObjectVirtualStringTreeData;
begin
   p := Self.GetNodeData(Node);
   if (Column = 0) then begin
      ImageIndex := p^.ImageIndex;
   end;
end;

procedure TObjectVirtualStringTreeClassHelper.ObjectDblClick(Sender: TObject);
var
   o: TObject;
   m: TMethod;
   n: PVirtualNode;
begin
   n := Self.GetFirstSelected;
   if not Assigned(n) then begin
      Exit;
   end;
   o := Self.GetObjectFromNode(n);
   try
      m := GetMethodProp(o, 'OnExecute');
      TNotifyEvent(m)(Sender);
   except
      // no method exists, so what...
   end;
end;

function TObjectVirtualStringTreeClassHelper.FindOrCreateColumn(AName: string): TVirtualTreeColumn;
var
   i: integer;
begin
   for i := 0 to Pred(Self.Header.Columns.Count) do begin
      if SameText(Self.Header.Columns[i].Text, AName) then begin
         Result := Self.Header.Columns[i];
         Exit;
      end;
   end;
   Result := AddColumn(AName);
end;

function TObjectVirtualStringTreeClassHelper.GetObjectFromNode(ANode: PVirtualNode): TObject;
var
   p: PObjectVirtualStringTreeData;
begin
   p := Self.GetNodeData(ANode);
   Result := p^.DataItem;
end;

function TObjectVirtualStringTreeClassHelper.GetSelectedItem<T>: T;
begin
   Result := nil;
   if Self.SelectedCount = 0 then begin
      Exit;
   end;
   Result := T(GetObjectFromNode(Self.GetFirstSelected()));
end;

procedure TObjectVirtualStringTreeClassHelper.SetSelectedItem<T>(AnItem: T);
var
   n: PVirtualNode;
   item: T;
   p: PObjectVirtualStringTreeData;
begin
   n := Self.GetFirst();
   while Assigned(n) do begin
      p := Self.GetNodeData(n);
      item := T(p^.DataItem);
      if AnItem = item then begin
         Self.Selected[n] := True;
         Self.FocusedNode := n;
         Exit;
      end;
      n := Self.GetNext(n);
   end;
end;

function TObjectVirtualStringTreeClassHelper.AddColumn(AName: string): TVirtualTreeColumn;
begin
   Result := Self.Header.Columns.Add;
   Result.Text := AName;
   Result.Width := 300;
end;

function TObjectVirtualStringTreeClassHelper.UpdateTreeDataForObject(AData: PObjectVirtualStringTreeData; AnItem: TObject): integer;
var
   context: TRttiContext;
   rType: TRttiType;
   sName: string;
   sData: string;
   lc: TVirtualTreeColumn;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   iProperty: integer;
   iAttribute: integer;
   attributeEntry: TAttributeEntry;
   attribute: TCustomAttribute;
   iGroupId: integer;
   iColumn: integer;
   aParent: ATreeViewParentIDField;
   aParentO: ATreeViewParentObjectField;
   aSkip: ATreeViewSkipField;
   aImageIndex: AOVMImageIndex;
   aButton: AOVMButton;
   bAdd: boolean;
   btn: TButton;
   m: TMethod;
begin
   Result := GetPropList(AnItem.ClassInfo, tkAny, @objectPropertyList, False);
   for iProperty := 0 to Pred(Result) do begin
      try
         objectPropertyInfo := objectPropertyList[iProperty];
         sName := OVMFieldNameToPublic(objectPropertyInfo^.Name);
         if Assigned(objectPropertyInfo^.AttributeTable) then begin
            for iAttribute := 0 to Pred(objectPropertyInfo^.AttributeTable.AttributeCount) do begin
               attributeEntry := objectPropertyInfo^.AttributeTable.AttributesList[iAttribute];
               attribute := attributeEntry.AttrProc;
               if attribute is AListViewColumnDetails then begin
                  sName := (attribute as AListViewColumnDetails).ColumnName;
               end;
            end;
         end;
         aImageIndex := AOVMImageIndex(GetPropAttributeFromTypInfo(objectPropertyInfo, AOVMImageIndex));
         aParent := ATreeViewParentIDField(GetPropAttributeFromTypInfo(objectPropertyInfo, ATreeViewParentIDField));
         aParentO := ATreeViewParentObjectField(GetPropAttributeFromTypInfo(objectPropertyInfo, ATreeViewParentObjectField));
         aButton := AOVMButton(GetPropAttributeFromTypInfo(objectPropertyInfo, AOVMButton));
         aSkip := ATreeViewSkipField(GetPropAttributeFromTypInfo(objectPropertyInfo, ATreeViewSkipField));
         bAdd := (not Assigned(aImageIndex)) and (not Assigned(aParent)) and (not Assigned(aParentO));
         if objectPropertyInfo^.PropType^.Kind in [tkMethod, tkClass] then begin
            bAdd := False;
         end;
         if Assigned(aSkip) then begin
            bAdd := false;
         end;
         if bAdd then begin
            lc := Self.FindOrCreateColumn(sName);
            sData := Trim(ObjectPropertyToDisplayText(AnItem, objectPropertyInfo));
            if (lc.Index = 0) then begin
               AData^.FirstText := sData;
            end;
            if AData^.ItemCount < Succ(lc.Index) then begin
               AData^.ItemCount := Succ(lc.Index);
               SetLength(AData^.ItemData, AData^.ItemCount);
               AData^.ItemData[lc.Index] := sData;
               SetLength(AData^.IsButton, AData^.ItemCount);
               AData^.IsButton[lc.Index] := Assigned(aButton);
               SetLength(AData^.Controls, AData^.ItemCount);
               if Assigned(aButton) and (Length(sData) > 0) then begin
                  btn := TButton.Create(nil);
                  btn.Parent := Self; // TODO : clean on refresh
                  btn.Height := Self.DefaultNodeHeight;
                  btn.Visible := False;
                  try
                     m := GetMethodProp(AnItem, 'OnExecute');
                     btn.OnClick := TNotifyEvent(m);
                     AData^.OnExecute := TNotifyEvent(m);
                  except
                     // no method exists, so what...
                  end;
                  AData^.Controls[lc.Index] := btn;
                  btn.Caption := sData;
               end else begin
                  AData^.Controls[lc.Index] := nil;
               end;
            end;
         end;
      except
         on E: Exception do begin
            DebugLogger.LogException(E, Format('TObjectListViewClassHelper.UpdateListItemForObject(x, %s)', [AnItem.ClassName]));
         end;
      end;
   end;
end;

function TObjectVirtualStringTreeClassHelper.GetItemIndexKey(TheSelectedNode: PVirtualNode; AnIdentifierClass: TCustomAttributeClass; ADefault: string): string;
var
   iCount: integer;
   iProperty: integer;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   a: TCustomAttribute;
   item: TObject;
   p: PObjectVirtualStringTreeData;
begin
   Result := ADefault;
   if not Assigned(TheSelectedNode) then begin
      Exit;
   end;
   p := PObjectVirtualStringTreeData(Self.GetNodeData(TheSelectedNode));
   item := p.DataItem;
   if not Assigned(item) then begin
      Exit;
   end;
   iCount := GetPropList(item.ClassInfo, tkAny, @objectPropertyList, False);
   for iProperty := 0 to Pred(iCount) do begin
      try
         objectPropertyInfo := objectPropertyList[iProperty];
         a := GetPropAttributeFromTypInfo(objectPropertyInfo, AnIdentifierClass);
         if Assigned(a) then begin
            Result := ObjectPropertyToDisplayText(item, objectPropertyInfo);
         end;
      except
         on E: Exception do begin
            DebugLogger.LogException(E, Format('TObjectVirtualStringTreeClassHelper.GetItemIndexKey(x, x, %s, %s)', [ADefault, item.ClassName]));
         end;
      end;
   end;
end;

procedure TObjectVirtualStringTreeClassHelper.DisplayItems<T>(TheItems: TEnumerable<T>);
var
   item: T;
   sText: string;
   iCount: integer;
   iProperty: integer;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   aDisplayText: ATreeViewDisplayText;
   aParent: ATreeViewParentIDField;
   aParentO: ATreeViewParentObjectField;
   aImageIndex: AOVMImageIndex;
   nParent: PVirtualNode;
   sParent: string;
   o: TObject;
   iImageIndex: integer;
   n: PVirtualNode;
   p: PObjectVirtualStringTreeData;
   selectedItem: T;
begin
   Self.NodeDataSize := SizeOf(TObjectVirtualStringTreeData);
   Self.OnGetText := Self.ObjectGetText;
   Self.OnGetImageIndex := Self.ObjectGetImageIndex;
   Self.OnAfterPaint := ObjectAfterPaint;
   if not Assigned(Self.OnDblClick) then begin
      Self.OnDblClick := Self.ObjectDblClick;
   end;
   selectedItem := Self.GetSelectedItem<T>();
   Self.BeginUpdate;
   try
      Self.Clear;
      sParent := '';
      for item in TheItems do begin
         sParent := '';
         sText := item.ClassName;
         iCount := GetPropList(item.ClassInfo, tkAny, @objectPropertyList, False);
         nParent := nil;
         iImageIndex := -1;
         for iProperty := 0 to Pred(iCount) do begin
            try
               objectPropertyInfo := objectPropertyList[iProperty];
               aDisplayText := ATreeViewDisplayText(GetPropAttributeFromTypInfo(objectPropertyInfo, ATreeViewDisplayText));
               if Assigned(aDisplayText) then begin
                  sText := ObjectPropertyToDisplayText(item, objectPropertyInfo);
               end;
               aImageIndex := AOVMImageIndex(GetPropAttributeFromTypInfo(objectPropertyInfo, AOVMImageIndex));
               if Assigned(aImageIndex) then begin
                  iImageIndex := GetOrdProp(item, objectPropertyInfo);
               end;
               aParent := ATreeViewParentIDField(GetPropAttributeFromTypInfo(objectPropertyInfo, ATreeViewParentIDField));
               if Assigned(aParent) then begin
                  sParent := ObjectPropertyToDisplayText(item, objectPropertyInfo);
               end;
               aParentO := ATreeViewParentObjectField(GetPropAttributeFromTypInfo(objectPropertyInfo, ATreeViewParentObjectField));
               if Assigned(aParentO) then begin
                  o := GetObjectProp(item, objectPropertyInfo);
                  nParent := Self.FindParentUsingObject<T>(T(o));
               end;
            except
               on E: Exception do begin
                  DebugLogger.LogException(E, Format('TObjectTreeViewClassHelper.DisplayItems(x, %s)', [item.ClassName]));
               end;
            end;
         end;
         if (sParent <> '') then begin
            nParent := Self.FindParentUsingKey<T>(sParent);
         end;
         n := Self.AddChild(nParent);
         p := Self.GetNodeData(n);
         p^.DataItem := item;
         p^.FirstText := sText;
         p^.ImageIndex := iImageIndex;
         Self.UpdateTreeDataForObject(p, item);
         Self.MultiLine[n] := True;
      end;
   finally
      Self.EndUpdate;
      Self.SetSelectedItem<T>(selectedItem);
   end;
end;

function TObjectVirtualStringTreeClassHelper.FindParentUsingKey<T>(AParentKey: string): PVirtualNode;
var
   i: integer;
   item: T;
   iCount: integer;
   iProperty: integer;
   sValue: string;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   a: ATreeViewUniqueIDField;
   n: PVirtualNode;
   p: PObjectVirtualStringTreeData;
begin
   Result := nil;
   n := Self.GetFirst();
   while Assigned(n) do begin
      p := Self.GetNodeData(n);
      item := T(p^.DataItem);
      iCount := GetPropList(item.ClassInfo, tkAny, @objectPropertyList, False);
      for iProperty := 0 to Pred(iCount) do begin
         try
            objectPropertyInfo := objectPropertyList[iProperty];
            a := ATreeViewUniqueIDField(GetPropAttributeFromTypInfo(objectPropertyInfo, ATreeViewUniqueIDField));
            if Assigned(a) then begin
               sValue := ObjectPropertyToDisplayText(item, objectPropertyInfo);
               if SameText(sValue, AParentKey) then begin
                  Result := n;
                  Exit;
               end;
            end;
         except
         end;
      end;
      n := Self.GetNext(n);
   end;
end;

function TObjectVirtualStringTreeClassHelper.FindParentUsingObject<T>(AParent: T): PVirtualNode;
var
   i: integer;
   item: T;
   n: PVirtualNode;
   p: PObjectVirtualStringTreeData;
begin
   Result := nil;
   n := Self.GetFirst();
   while Assigned(n) do begin
      p := PObjectVirtualStringTreeData(Self.GetNodeData(n));
      item := T(p^.DataItem);
      if (AParent = item) then begin
         Result := n;
         Exit;
      end;
      n := Self.GetNext(n);
   end;
end;

function TObjectVirtualStringTreeClassHelper.GetSelectedKey(ADefault: string): string;
begin
   Result := GetItemIndexKey(Self.GetFirstSelected(), ATreeViewUniqueIDField, ADefault);
end;

function TObjectVirtualStringTreeClassHelper.GetSelectedParentKey(ADefault: string): string;
begin
   Result := GetItemIndexKey(Self.GetFirstSelected(), ATreeViewParentIDField, ADefault);
end;

procedure TObjectVirtualStringTreeClassHelper.SetNodeControlVisible(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex = NoColumn);
var
   NodeData: PObjectVirtualStringTreeData;
   R: TRect;
   iColumn: integer;
begin
   //if (Column = NoColumn) then begin
   //   Exit;
   //end;
   NodeData := Tree.GetNodeData(Node);
   if Assigned(NodeData) then begin
      for iColumn := 0 to Pred(Self.Header.Columns.Count) do begin
         if Assigned(NodeData^.Controls[iColumn]) then begin
            with NodeData.Controls[iColumn] do begin
               Visible := IsNodeVisibleInClientRect(Node, iColumn) and ((Node.Parent = Tree.RootNode) or (vsExpanded in Node.Parent.States));
               R := Tree.GetDisplayRect(Node, iColumn, False);
               InflateRect(R, -10, -2);
               BoundsRect := R;
            end;
         end;
      end;
   end;
end;

function TObjectVirtualStringTreeClassHelper.IsNodeVisibleInClientRect(Node: PVirtualNode; Column: TColumnIndex = NoColumn): boolean;
begin
   Result := Self.IsVisible[Node] and Self.GetDisplayRect(Node, Column, False).IntersectsWith(Self.ClientRect);
end;

procedure TObjectVirtualStringTreeClassHelper.SetNodesControlVisibleProc(Sender: TBaseVirtualTree; Node: PVirtualNode; Data: Pointer; var Abort: boolean);
begin
   SetNodeControlVisible(Sender, Node);
end;

procedure TObjectVirtualStringTreeClassHelper.ObjectAfterPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas);
begin
   // Iterate all Tree nodes and set visibility
   Sender.IterateSubtree(nil, SetNodesControlVisibleProc, nil);
end;

end.
