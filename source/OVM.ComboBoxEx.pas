{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(ComboBox enhancement.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-08-04  pk   10m  Created new support for TComboBox
// *****************************************************************************
   )
}
unit OVM.ComboBoxEx;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$modeswitch prefixedattributes}
//{$modeswitch multiscopehelpers}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   StdCtrls,
   ComboEx,
   Generics.Collections,
   TypInfo,
   DebugLog,
   OVM.Basics,
   OVM.Attributes,
   OVM.ComboBox.Attributes;

type

   { TObjectComboBoxExClassHelper }

   TObjectComboBoxExClassHelper = class helper for TComboBoxEx
   public
      function GetItemIndexKey(TheItemIndex: integer; AnIdentifierClass: TCustomAttributeClass; ADefault: string = ''): string;
      procedure DisplayItems<T: class>(TheItems: TEnumerable<T>); overload;
      function FindParent<T: class>(AParentKey: string): TComboExItem;
      function GetSelectedKey(ADefault: string = ''): string;
      function GetSelectedParentKey(ADefault: string = ''): string;
      function GetSelectedItem<T: class>: T;
      procedure SetSelectedItem<T: class>(AnItem: T);
   end;

implementation

{ TObjectComboBoxExClassHelper }

function TObjectComboBoxExClassHelper.GetItemIndexKey(TheItemIndex: integer; AnIdentifierClass: TCustomAttributeClass; ADefault: string): string;
var
   iCount: integer;
   iProperty: integer;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   a: TCustomAttribute;
   item: TObject;
begin
   Result := ADefault;
   if TheItemIndex < 0 then begin
      Exit;
   end;
   item := TObject(Self.ItemsEx[TheItemIndex].Data);
   if not Assigned(item) then begin
      Exit;
   end;
   iCount := GetPropList(item.ClassInfo, tkAny, @objectPropertyList, False);
   for iProperty := 0 to Pred(iCount) do begin
      try
         objectPropertyInfo := objectPropertyList[iProperty];
         a := GetPropAttributeFromTypInfo(objectPropertyInfo, AnIdentifierClass);
         if Assigned(a) then begin
            Result := ObjectPropertyToDisplayText(item, objectPropertyInfo);
         end;
      except
         on E: Exception do begin
            DebugLogger.LogException(E, Format('TObjectComboBoxExClassHelper.GetItemIndexKey(%d, %s)', [TheItemIndex, item.ClassName]));
         end;
      end;
   end;
end;

procedure TObjectComboBoxExClassHelper.DisplayItems<T>(TheItems: TEnumerable<T>);
var
   item: T;
   sText: string;
   iCount: integer;
   iProperty: integer;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   aDisplayText: AComboBoxDisplayText;
   aParent: AComboBoxParentIDField;
   aImageIndex: AOVMImageIndex;
   ceiItem: TComboExItem;
   ceiParent: TComboExItem;
   sParent: string;
   iImageIndex: integer;
   selectedItem: T;
begin
   selectedItem := Self.GetSelectedItem<T>();
   Self.Items.BeginUpdate;
   try
      Self.Items.Clear;
      sParent := '';
      for item in TheItems do begin
         sParent := '';
         sText := item.ClassName;
         iCount := GetPropList(item.ClassInfo, tkAny, @objectPropertyList, False);
         iImageIndex := -1;
         for iProperty := 0 to Pred(iCount) do begin
            try
               objectPropertyInfo := objectPropertyList[iProperty];
               aDisplayText := AComboBoxDisplayText(GetPropAttributeFromTypInfo(objectPropertyInfo, AComboBoxDisplayText));
               if Assigned(aDisplayText) then begin
                  sText := ObjectPropertyToDisplayText(item, objectPropertyInfo);
               end;
               aParent := AComboBoxParentIDField(GetPropAttributeFromTypInfo(objectPropertyInfo, AComboBoxParentIDField));
               if Assigned(aParent) then begin
                  sParent := ObjectPropertyToDisplayText(item, objectPropertyInfo);
               end;
               aImageIndex := AOVMImageIndex(GetPropAttributeFromTypInfo(objectPropertyInfo, AOVMImageIndex));
               if Assigned(aImageIndex) then begin
                  iImageIndex := GetOrdProp(item, objectPropertyInfo);
               end;
            except
               on E: Exception do begin
                  DebugLogger.LogException(E, Format('TObjectComboBoxExClassHelper.DisplayItems(x, %s)', [item.ClassName]));
               end;
            end;
         end;
         ceiItem := Self.ItemsEx.Add;
         ceiItem.Caption := sText;
         ceiItem.Data := Pointer(item);
         ceiItem.ImageIndex := iImageIndex;
         if (sParent <> '') then begin
            ceiParent := Self.FindParent<T>(sParent);
            if Assigned(ceiParent) then begin
               ceiItem.Indent := ceiParent.Indent + 10;
            end;
         end;
      end;
   finally
      Self.Items.EndUpdate;
      Self.SetSelectedItem<T>(selectedItem);
   end;
end;

function TObjectComboBoxExClassHelper.FindParent<T>(AParentKey: string): TComboExItem;
var
   i: integer;
   item: T;
   iCount: integer;
   iProperty: integer;
   sValue: string;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   a: AComboBoxUniqueIDField;
begin
   Result := nil;
   for i := 0 to Pred(Self.ItemsEx.Count) do begin
      item := T(Self.ItemsEx[i].Data);
      iCount := GetPropList(item.ClassInfo, tkAny, @objectPropertyList, False);
      for iProperty := 0 to Pred(iCount) do begin
         try
            objectPropertyInfo := objectPropertyList[iProperty];
            a := AComboBoxUniqueIDField(GetPropAttributeFromTypInfo(objectPropertyInfo, AComboBoxUniqueIDField));
            if Assigned(a) then begin
               sValue := ObjectPropertyToDisplayText(item, objectPropertyInfo);
               if SameText(sValue, AParentKey) then begin
                  Result := Self.ItemsEx[i];
                  Exit;
               end;
            end;
         except
         end;
      end;
   end;
end;

function TObjectComboBoxExClassHelper.GetSelectedKey(ADefault: string): string;
begin
   Result := GetItemIndexKey(Self.ItemIndex, AComboBoxUniqueIDField, ADefault);
end;

function TObjectComboBoxExClassHelper.GetSelectedParentKey(ADefault: string): string;
begin
   Result := GetItemIndexKey(Self.ItemIndex, AComboBoxParentIDField, ADefault);
end;

function TObjectComboBoxExClassHelper.GetSelectedItem<T>: T;
begin
   Result := nil;
   if Self.ItemIndex < 0 then begin
      Exit;
   end;
   Result := T(Self.ItemsEx[Self.ItemIndex].Data);
end;

procedure TObjectComboBoxExClassHelper.SetSelectedItem<T>(AnItem: T);
var
   i: integer;
   item: T;
   ceiItem: TComboExItem;
begin
   if not Assigned(AnItem) then begin
      Exit;
   end;
   for i := 0 to Pred(Self.ItemsEx.Count) do begin
      ceiItem := Self.ItemsEx[i];
      item := T(ceiItem.Data);
      if item = AnItem then begin
         Self.ItemIndex := i;
         Exit;
      end;
   end;
end;

end.
