{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(TreeView enhancement.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-08-04  pk   10m  Created new support for TTreeView
// *****************************************************************************
   )
}
unit OVM.TreeView;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$modeswitch prefixedattributes}
//{$modeswitch multiscopehelpers}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   ComCtrls,
   Generics.Collections,
   TypInfo,
   DebugLog,
   OVM.Basics,
   OVM.Attributes,
   OVM.TreeView.Attributes;

type
   { TObjectTreeViewClassHelper }

   TObjectTreeViewClassHelper = class helper for TTreeView
   private
      procedure ObjectDblClick(Sender: TObject);
   public
      function GetItemIndexKey(TheSelectedNode: TTreeNode; AnIdentifierClass: TCustomAttributeClass; ADefault: string = ''): string;
      procedure DisplayItems<T: class>(TheItems: TEnumerable<T>); overload;
      function FindParentUsingKey<T: class>(AParentKey: string): TTreeNode;
      function FindParentUsingObject<T: class>(AParent: T): TTreeNode;
      function GetSelectedKey(ADefault: string = ''): string;
      function GetSelectedParentKey(ADefault: string = ''): string;
      function GetSelectedItem<T: class>: T;
      procedure SetSelectedItem<T: class>(AnItem: T);
   end;

implementation

{ TObjectTreeViewClassHelper }

procedure TObjectTreeViewClassHelper.ObjectDblClick(Sender: TObject);
var
   o: TObject;
   m: TMethod;
   n: TTreeNode;
begin
   n := Self.Selected;
   if not Assigned(n) then begin
      Exit;
   end;
   o := TObject(n.Data);
   try
      m := GetMethodProp(o, 'OnExecute');
      TNotifyEvent(m)(Sender);
   except
      // no method exists, so what...
   end;
end;

function TObjectTreeViewClassHelper.GetItemIndexKey(TheSelectedNode: TTreeNode; AnIdentifierClass: TCustomAttributeClass; ADefault: string): string;
var
   iCount: integer;
   iProperty: integer;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   a: TCustomAttribute;
   item: TObject;
begin
   Result := ADefault;
   if not Assigned(TheSelectedNode) then begin
      Exit;
   end;
   item := TObject(TheSelectedNode.Data);
   if not Assigned(item) then begin
      Exit;
   end;
   iCount := GetPropList(item.ClassInfo, tkAny, @objectPropertyList, False);
   for iProperty := 0 to Pred(iCount) do begin
      try
         objectPropertyInfo := objectPropertyList[iProperty];
         a := GetPropAttributeFromTypInfo(objectPropertyInfo, AnIdentifierClass);
         if Assigned(a) then begin
            Result := ObjectPropertyToDisplayText(item, objectPropertyInfo);
         end;
      except
         on E: Exception do begin
            DebugLogger.LogException(E, Format('TObjectTreeViewClassHelper.GetItemIndexKey(x, x, %s, %s)', [ADefault, item.ClassName]));
         end;
      end;
   end;
end;

procedure TObjectTreeViewClassHelper.DisplayItems<T>(TheItems: TEnumerable<T>);
var
   item: T;
   sText: string;
   iCount: integer;
   iProperty: integer;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   aDisplayText: ATreeViewDisplayText;
   aParent: ATreeViewParentIDField;
   aParentO: ATreeViewParentObjectField;
   aImageIndex: AOVMImageIndex;
   tnParent: TTreeNode;
   sParent: string;
   o: TObject;
   iImageIndex: integer;
   tn: TTreeNode;
   selectedItem: T;
begin
   if not Assigned(Self.OnDblClick) then begin
      Self.OnDblClick := Self.ObjectDblClick;
   end;
   selectedItem := Self.GetSelectedItem<T>();
   Self.Items.BeginUpdate;
   try
      Self.Items.Clear;
      sParent := '';
      for item in TheItems do begin
         sParent := '';
         sText := item.ClassName;
         iCount := GetPropList(item.ClassInfo, tkAny, @objectPropertyList, False);
         tnParent := nil;
         iImageIndex := -1;
         for iProperty := 0 to Pred(iCount) do begin
            try
               objectPropertyInfo := objectPropertyList[iProperty];
               aDisplayText := ATreeViewDisplayText(GetPropAttributeFromTypInfo(objectPropertyInfo, ATreeViewDisplayText));
               if Assigned(aDisplayText) then begin
                  sText := ObjectPropertyToDisplayText(item, objectPropertyInfo);
               end;
               aImageIndex := AOVMImageIndex(GetPropAttributeFromTypInfo(objectPropertyInfo, AOVMImageIndex));
               if Assigned(aImageIndex) then begin
                  iImageIndex := GetOrdProp(item, objectPropertyInfo);
               end;
               aParent := ATreeViewParentIDField(GetPropAttributeFromTypInfo(objectPropertyInfo, ATreeViewParentIDField));
               if Assigned(aParent) then begin
                  sParent := ObjectPropertyToDisplayText(item, objectPropertyInfo);
               end;
               aParentO := ATreeViewParentObjectField(GetPropAttributeFromTypInfo(objectPropertyInfo, ATreeViewParentObjectField));
               if Assigned(aParentO) then begin
                  o := GetObjectProp(item, objectPropertyInfo);
                  tnParent := Self.FindParentUsingObject<T>(T(o));
               end;
            except
               on E: Exception do begin
                  DebugLogger.LogException(E, Format('TObjectTreeViewClassHelper.DisplayItems(x, %s)', [item.ClassName]));
               end;
            end;
         end;
         if (sParent <> '') then begin
            tnParent := Self.FindParentUsingKey<T>(sParent);
         end;
         tn := Self.Items.AddChildObject(tnParent, sText, Pointer(item));
         tn.ImageIndex := iImageIndex;
         tn.SelectedIndex := iImageIndex;
      end;
   finally
      Self.Items.EndUpdate;
      Self.SetSelectedItem<T>(selectedItem);
   end;
end;

function TObjectTreeViewClassHelper.FindParentUsingObject<T>(AParent: T): TTreeNode;
var
   i: integer;
   item: T;
begin
   Result := nil;
   for i := 0 to Pred(Self.Items.Count) do begin
      item := T(Self.Items[i].Data);
      if (AParent = item) then begin
         Result := Self.Items[i];
         Exit;
      end;
   end;
end;

function TObjectTreeViewClassHelper.FindParentUsingKey<T>(AParentKey: string): TTreeNode;
var
   i: integer;
   item: T;
   iCount: integer;
   iProperty: integer;
   sValue: string;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   a: ATreeViewUniqueIDField;
begin
   Result := nil;
   for i := 0 to Pred(Self.Items.Count) do begin
      item := T(Self.Items[i].Data);
      iCount := GetPropList(item.ClassInfo, tkAny, @objectPropertyList, False);
      for iProperty := 0 to Pred(iCount) do begin
         try
            objectPropertyInfo := objectPropertyList[iProperty];
            a := ATreeViewUniqueIDField(GetPropAttributeFromTypInfo(objectPropertyInfo, ATreeViewUniqueIDField));
            if Assigned(a) then begin
               sValue := ObjectPropertyToDisplayText(item, objectPropertyInfo);
               if SameText(sValue, AParentKey) then begin
                  Result := Self.Items[i];
                  Exit;
               end;
            end;
         except
         end;
      end;
   end;
end;

function TObjectTreeViewClassHelper.GetSelectedKey(ADefault: string): string;
begin
   Result := GetItemIndexKey(Self.Selected, ATreeViewUniqueIDField, ADefault);
end;

function TObjectTreeViewClassHelper.GetSelectedParentKey(ADefault: string): string;
begin
   Result := GetItemIndexKey(Self.Selected, ATreeViewParentIDField, ADefault);
end;

function TObjectTreeViewClassHelper.GetSelectedItem<T>: T;
begin
   Result := nil;
   if not Assigned(Self.Selected) then begin
      Exit;
   end;
   Result := T(Self.Selected.Data);
end;

procedure TObjectTreeViewClassHelper.SetSelectedItem<T>(AnItem: T);
var
   li: TTreeNode;
   i: integer;
begin
   if not Assigned(AnItem) then begin
      Exit;
   end;
   for i := 0 to Pred(Self.Items.Count) do begin
      li := Self.Items[i];
      if T(li.Data) = AnItem then begin
         Self.Selected := li;
         Exit;
      end;
   end;
end;

end.
