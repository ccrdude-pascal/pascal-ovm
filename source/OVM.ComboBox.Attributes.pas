{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Class and property attributes to manage how they are displayed in TComboBox.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-25  pk  ---  Created new unit for new TComboBox support.
// *****************************************************************************
   )
}

unit OVM.ComboBox.Attributes;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$modeswitch prefixedattributes}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils;

type

   { AComboBoxDisplayText }

   AComboBoxDisplayText = class(TCustomAttribute)
   public
      constructor Create();
   end;

   { AComboBoxUniqueIDField }

   AComboBoxUniqueIDField = class(TCustomAttribute)
   public
      constructor Create();
   end;

   { AComboBoxParentIDField }

   AComboBoxParentIDField = class(TCustomAttribute)
   public
      constructor Create();
   end;

implementation

{ AComboBoxDisplayText }

constructor AComboBoxDisplayText.Create();
begin

end;

{ AComboBoxUniqueIDField }

constructor AComboBoxUniqueIDField.Create();
begin

end;

{ AComboBoxParentIDField }

constructor AComboBoxParentIDField.Create();
begin

end;

end.
