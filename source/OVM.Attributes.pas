{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Generic attributes to help with displaying data.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// *****************************************************************************
   )
}

unit OVM.Attributes;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils;

type

   { AOVMImageIndex }

   AOVMImageIndex = class(TCustomAttribute)
   public
      constructor Create();
   end;

   { AOVMBooleanImageIndex }

   AOVMBooleanImageIndex = class(TCustomAttribute)
   private
      FImageIndexFalse: integer;
      FImageIndexTrue: integer;
   public
      constructor Create(AImageIndexTrue, AImageIndexFalse: integer);
      property ImageIndexTrue: integer read FImageIndexTrue;
      property ImageIndexFalse: integer read FImageIndexFalse;
   end;

   { AOVMButton }

   AOVMButton = class(TCustomAttribute)
   public
      constructor Create();
   end;

   { AOVMBooleanYesNo }

   AOVMBooleanYesNo = class(TCustomAttribute)
   private
      FTextFalse: string;
      FTextTrue: string;
   public
      constructor Create();
      property TextTrue: string read FTextTrue;
      property TextFalse: string read FTextFalse;
   end;

   { AOVMBooleanStringValues }

   AOVMBooleanStringValues = class(TCustomAttribute)
   private
      FTextFalse: string;
      FTextTrue: string;
   public
      constructor Create(ATextTrue, ATextFalse: string);
      property TextTrue: string read FTextTrue;
      property TextFalse: string read FTextFalse;
   end;

implementation

uses
   Firefly.i18n;

   { AOVMImageIndex }

constructor AOVMImageIndex.Create();
begin

end;

{ AOVMBooleanImageIndex }

constructor AOVMBooleanImageIndex.Create(AImageIndexTrue, AImageIndexFalse: integer);
begin
   FImageIndexTrue := AImageIndexTrue;
   FImageIndexFalse := AImageIndexFalse;
end;

{ AOVMButton }

constructor AOVMButton.Create();
begin

end;

{ AOVMBooleanYesNo }

constructor AOVMBooleanYesNo.Create();
begin
   FTextTrue := _('Yes');
   FTextFalse := _('No');
end;

{ AOVMBooleanStringValues }

constructor AOVMBooleanStringValues.Create(ATextTrue, ATextFalse: string);
begin
   FTextTrue := ATextTrue;
   FTextFalse := ATextFalse;
end;

end.
