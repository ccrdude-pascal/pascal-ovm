unit OVMDemo.DemoData.TreeView;

{$mode Delphi}{$H+}{$M+}

interface

uses
   Classes,
   SysUtils,
   Dialogs,
   Generics.Collections,
   OVM.Attributes,
   OVM.TreeView.Attributes;

type
   TDemoDataType = (ddtFolder, ddtSomething);

   { TreeViewDemoData }

   TreeViewDemoData = class
   private
      FAction: string;
      FOnExecute: TNotifyEvent;
      FParent: TreeViewDemoData;
      FSubText: string;
      FTemplateType: TDemoDataType;
      FTitle: string;
      function GetImageIndex: integer;
      procedure Execute(Sender: TObject);
   public
      constructor Create;
      property DataType: TDemoDataType read FTemplateType;
   published
      [ATreeViewDisplayText()]
      property Title: string read FTitle;
      property SubText: string read FSubText;
      [ATreeViewParentObjectField()]
      property Parent: TreeViewDemoData read FParent;
      [AOVMImageIndex()]
      property ImageIndex: integer read GetImageIndex;
      [AOVMButton()]
      property Action: string read FAction;
      property OnExecute: TNotifyEvent read FOnExecute;
   end;

   { TreeViewDemoDataList }

   TreeViewDemoDataList = class(TObjectList<TreeViewDemoData>)
   public
      procedure CreateTestData;
   end;

implementation

{ TreeViewDemoData }

function TreeViewDemoData.GetImageIndex: integer;
begin
   case FTemplateType of
      ddtFolder: begin
         Result := 0;
      end;
      else
      begin
         Result := 1;
      end;
   end;
end;

constructor TreeViewDemoData.Create;
begin
   Self.FOnExecute := Execute;
end;

procedure TreeViewDemoData.Execute(Sender: TObject);
begin
   MessageDlg('TreeViewDemoData clicked', Self.Title, mtInformation, [mbOK], 0);
end;

{ TreeViewDemoDataList }

procedure TreeViewDemoDataList.CreateTestData;
var
   folder: TreeViewDemoData;
   item: TreeViewDemoData;
begin
   folder := TreeViewDemoData.Create;
   folder.FTitle := 'Folder 1';
   folder.FSubText := 'English';
   folder.FTemplateType := ddtFolder;
   Self.Add(folder);
   item := TreeViewDemoData.Create;
   item.FTitle := 'Item 1.1';
   item.FSubText := 'Hello World';
   item.FTemplateType := ddtSomething;
   item.FAction := 'Greet';
   item.FParent := folder;
   Self.Add(item);
   item := TreeViewDemoData.Create;
   item.FTitle := 'Item 1.2';
   item.FSubText := 'Hello Worlds';
   item.FTemplateType := ddtSomething;
   item.FParent := folder;
   Self.Add(item);
   folder := TreeViewDemoData.Create;
   folder.FTitle := 'Folder 2';
   folder.FSubText := 'Deutsch';
   folder.FTemplateType := ddtFolder;
   Self.Add(folder);
   item := TreeViewDemoData.Create;
   item.FTitle := 'Item 2.1';
   item.FSubText := 'Hallo Welt';
   item.FTemplateType := ddtSomething;
   item.FAction := 'Grüße';
   item.FParent := folder;
   Self.Add(item);
   item := TreeViewDemoData.Create;
   item.FTitle := 'Item 2.2';
   item.FSubText := 'Hallo Welten';
   item.FTemplateType := ddtSomething;
   item.FParent := folder;
   Self.Add(item);
   item := TreeViewDemoData.Create;
   item.FTitle := 'Langer Text';
   item.FSubText := 'Franz jagt im komplett verwahrlosten Taxi quer durch Bayern. Bei jedem klugen Wort von Sokrates rief Xanthippe zynisch: Quatsch! Xaver schreibt für Wikipedia zum Spaß quälend lang über Yoga, Soja und Öko.';
   item.FTemplateType := ddtSomething;
   item.FParent := folder;
   Self.Add(item);
end;

end.
