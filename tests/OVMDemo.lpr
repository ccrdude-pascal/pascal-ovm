{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.@kolla@safer-networking.org>)
   @abstract(Demo project to demonstrate the Object Visualization Mapping units.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-23  pk  ---  Added header.
// *****************************************************************************
   )
}

program OVMDemo;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

uses
   {$IFDEF UNIX}
   cthreads,
   {$ENDIF}
   {$IFDEF HASAMIGA}
   athreads,
   {$ENDIF}
   Interfaces, // this includes the LCL widgetset
   Forms,
   DebugLog,
   OVMDemo.UI.FormMain,
   OVMDemo.DemoData.TreeView;

   {$R *.res}

begin
   DebugLogger.LogEnterProcess('OVMDemo');
   try
      RequireDerivedFormResource := True;
      Application.Scaled := True;
      Application.Initialize;
      Application.CreateForm(TFormOVM, FormOVM);
      Application.Run;
   finally
      DebugLogger.LogLeaveProcess('OMVDemo');
   end;
end.
