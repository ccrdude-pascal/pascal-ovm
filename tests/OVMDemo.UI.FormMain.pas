{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Form to display test data for the Object Visualization Mapping units.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-23  pk  Fixed call to DisplayEnumerable.
// 2023-03-23  pk  Added header.
// *****************************************************************************
   )
}

unit OVMDemo.UI.FormMain;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ComCtrls,
   ExtCtrls,
   StdCtrls,
   ComboEx,
   TypInfo,
   Generics.Collections,
   OVMDemo.DemoData.ListView,
   OVMDemo.DemoData.TreeView,
   OVM.ListView,
   OVM.TreeView,
   OVM.ComboBox,
   OVM.ComboBoxEx,
   OVM.VirtualTreeView,
   VirtualTrees;

type

   { TFormOVM }

   TFormOVM = class(TForm)
      ComboBox1: TComboBox;
      ComboBoxEx1: TComboBoxEx;
      ImageList1: TImageList;
      lvProperties: TListView;
      lv: TListView;
      PageControl1: TPageControl;
      splitterProperties: TSplitter;
      tabListView: TTabSheet;
      tabComboBox: TTabSheet;
      tabVirtualStringTree: TTabSheet;
      tabTreeView: TTabSheet;
      tbObjectListTreeViewUsingParentObjects: TToolButton;
      tbObjectListVirtualStringTreeUsingParentObjects: TToolButton;
      tbObjectListComboBox: TToolButton;
      toolbarListView: TToolBar;
      tbObjectList: TToolButton;
      tbList: TToolButton;
      tbCollection: TToolButton;
      toolbarTreeView: TToolBar;
      toolbarVirtualStringTree: TToolBar;
      toolbarVirtualStringTree1: TToolBar;
      tbEnumerator: TToolButton;
      tv: TTreeView;
      vst: TVirtualStringTree;
      procedure ComboBox1Change(Sender: TObject);
      procedure ComboBoxEx1Change(Sender: TObject);
      procedure FormCreate({%H-}Sender: TObject);
      procedure FormDestroy({%H-}Sender: TObject);
      procedure lvSelectItem({%H-}Sender: TObject; Item: TListItem; Selected: boolean);
      procedure tbCollectionClick({%H-}Sender: TObject);
      procedure tbEnumeratorClick(Sender: TObject);
      procedure tbListClick({%H-}Sender: TObject);
      procedure tbObjectListClick({%H-}Sender: TObject);
      procedure tbObjectListComboBoxClick(Sender: TObject);
      procedure tbObjectListTreeViewUsingParentObjectsClick(Sender: TObject);
      procedure tbObjectListVirtualStringTreeUsingParentObjectsClick(Sender: TObject);
      procedure tvSelectionChanged(Sender: TObject);
      procedure vstFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
      procedure vstMeasureItem(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; var NodeHeight: integer);
   private
      FObjectListForListView: TDemoObjectList;
      FListForListView: TDemoSimpleList;
      FCollectionForListView: TDemoCollection;
      FObjectListForTreeView: TreeViewDemoDataList;
   public
   end;

var
   FormOVM: TFormOVM;

implementation

{$R *.lfm}

{ TFormOVM }

procedure TFormOVM.FormCreate(Sender: TObject);
begin
   FObjectListForListView := TDemoObjectList.Create;
   FObjectListForListView.CreateTestData;
   FListForListView := TDemoSimpleList.Create;
   FListForListView.CreateTestData;
   FObjectListForTreeView := TreeViewDemoDataList.Create;
   FObjectListForTreeView.CreateTestData;
   // TODO : maybe an option to display these
   //FStringList := TDemoStringList.Create;
   //FStringList.Free;
   FCollectionForListView := TDemoCollection.Create;
   FCollectionForListView.CreateTestData;
end;

procedure TFormOVM.ComboBox1Change(Sender: TObject);
begin
   if ComboBox1.ItemIndex > -1 then begin
      lvProperties.DisplayProperties(TObject(ComboBox1.Items.Objects[ComboBox1.ItemIndex]));
   end;
end;

procedure TFormOVM.ComboBoxEx1Change(Sender: TObject);
begin
   if ComboBoxEx1.ItemIndex > -1 then begin
      lvProperties.DisplayProperties(TObject(ComboBoxEx1.ItemsEx[ComboBoxEx1.ItemIndex].Data));
   end;
end;

procedure TFormOVM.FormDestroy(Sender: TObject);
begin
   FObjectListForListView.Free;
   FListForListView.Free;
   //FStringList.Free;
   FCollectionForListView.Free;
end;

procedure TFormOVM.lvSelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
begin
   if Assigned(Item) and Selected then begin
      lvProperties.DisplayProperties(TObject(Item.Data));
   end;
end;

procedure TFormOVM.tbCollectionClick(Sender: TObject);
begin
   lv.DisplayItems(FCollectionForListView);
end;

procedure TFormOVM.tbEnumeratorClick(Sender: TObject);
var
   enum: TDemoSimpleListEnumerator;
begin
   enum := FListForListView.GetEnumerator;
   try
      lv.DisplayItems<TDemoObjectListItem>(enum);
   finally
      enum.Free;
   end;
end;

procedure TFormOVM.tbListClick(Sender: TObject);
begin
   lv.DisplayItems<TDemoObjectListItem>(FListForListView);
end;

procedure TFormOVM.tbObjectListClick(Sender: TObject);
begin
   lv.DisplayItems<TDemoObjectListItem>(FObjectListForListView);
end;

procedure TFormOVM.tbObjectListComboBoxClick(Sender: TObject);
begin
   ComboBox1.DisplayItems<TDemoObjectListItem>(FObjectListForListView);
   ComboBoxEx1.DisplayItems<TDemoObjectListItem>(FObjectListForListView);
end;

procedure TFormOVM.tbObjectListTreeViewUsingParentObjectsClick(Sender: TObject);
begin
   tv.DisplayItems<TreeViewDemoData>(FObjectListForTreeView);
   tv.FullExpand;
end;

procedure TFormOVM.tbObjectListVirtualStringTreeUsingParentObjectsClick(Sender: TObject);
begin
   vst.DisplayItems<TreeViewDemoData>(FObjectListForTreeView);
   vst.FullExpand();
end;

procedure TFormOVM.tvSelectionChanged(Sender: TObject);
begin
   if Assigned(tv.Selected) then begin
      lvProperties.DisplayProperties(TObject(tv.Selected.Data));
   end;
end;

procedure TFormOVM.vstFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
begin
   lvProperties.DisplayProperties(vst.GetObjectFromNode(vst.GetFirstSelected));
end;

procedure TFormOVM.vstMeasureItem(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; var NodeHeight: integer);
begin
   NodeHeight := Round(TargetCanvas.TextHeight('X') * 3.3);
end;

end.
