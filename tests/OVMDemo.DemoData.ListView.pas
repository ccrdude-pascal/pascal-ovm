unit OVMDemo.DemoData.ListView;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Generics.Collections,
   Dialogs,
   OVM.Attributes,
   OVM.ComboBox.Attributes,
   OVM.ListView.Attributes;

type
   TDemoObjectListItemMinAge = (dolin15Years, dolin18Years, dolin21Years);

   { TDemoObjectListItem }

   [AListViewGrouping(True, 'Media')]
   TDemoObjectListItem = class
   private
      FOnExecute: TNotifyEvent;
      FDescription: string;
      FMedia: string;
      FMinimumAge: TDemoObjectListItemMinAge;
      FReleaseDate: TDate;
      FTitle: string;
      function GetImageIndex: integer;
      procedure Execute(Sender: TObject);
   public
      constructor Create;
   published
      [AComboBoxDisplayText()]
      property Title: string read FTitle write FTitle;
      [AListViewColumnDetails('Description')]
      property Description: string read FDescription write FDescription;
      [AListViewColumnDetails('Release')]
      property ReleaseDate: TDate read FReleaseDate write FReleaseDate;
      [AListViewColumnDetails('Media')]
      property Media: string read FMedia write FMedia;
      property MinimumAge: TDemoObjectListItemMinAge read FMinimumAge write FMinimumAge;
      [AOVMImageIndex()]
      property ImageIndex: integer read GetImageIndex;
      property OnExecute: TNotifyEvent read FOnExecute;
   end;

   { TDemoObjectList }

   TDemoObjectList = class(TObjectList<TDemoObjectListItem>)
   public
      procedure CreateTestData;
   end;

   TDemoSimpleListEnumerator = class;

   { TDemoSimpleList }

   [AListViewGrouping(True, 'Media')]
   TDemoSimpleList = class(TList<TDemoObjectListItem>)
   protected
      procedure Notify(const AValue: TDemoObjectListItem; ACollectionNotification: TCollectionNotification); override;
   public
      destructor Destroy; override;
      function GetEnumerator: TDemoSimpleListEnumerator;
      procedure CreateTestData;
   end;

   { TDemoSimpleListEnumerator }

   TDemoSimpleListEnumerator = class(TEnumerator<TDemoObjectListItem>)
   private
      FList: TDemoSimpleList;
      FPosition: integer;
      function DoGetCurrent: TDemoObjectListItem; override;
      function DoMoveNext: boolean; override;
   public
      constructor Create(AList: TDemoSimpleList);
   end;

   TDemoCollectionItemType = (dcitFamily, dcitFriends, dcitCustomer, dcitOther);

   { TDemoCollectionItem }

   [AListViewGrouping(True, 'Category')]
   TDemoCollectionItem = class(TCollectionItem)
   private
      FCategory: TDemoCollectionItemType;
      FFirstName: string;
      FID: integer;
      FLastName: string;
   published
      [AListViewColumnDetails('Person ID'), APropertyListViewGroup('IDs')]
      property ID: integer read FID write FID;
      [AListViewColumnDetails('Family Name'), AListViewGroupColumn, APropertyListViewGroup('Name')]
      property LastName: string read FLastName write FLastName;
      [APropertyListViewGroup('Name')]
      property FirstName: string read FFirstName write FFirstName;
      property Category: TDemoCollectionItemType read FCategory write FCategory;
   end;

   { TDemoCollection }

   TDemoCollection = class(TCollection)
   public
      constructor Create;
      procedure CreateTestData;
   end;

   { TDemoStringList }

   TDemoStringList = class(TList<string>)
   public
      procedure CreateTestData;
   end;

implementation

{ TDemoCollection }

constructor TDemoCollection.Create;
begin
   inherited Create(TDemoCollectionItem);
end;

procedure TDemoCollection.CreateTestData;
var
   ci: TDemoCollectionItem;
begin
   ci := TDemoCollectionItem(Self.Add);
   ci.ID := 1;
   ci.LastName := 'Doe';
   ci.FirstName := 'John';
   ci.Category := dcitFamily;

   ci := TDemoCollectionItem(Self.Add);
   ci.ID := 2;
   ci.LastName := 'Doe';
   ci.FirstName := 'Jane';
   ci.Category := dcitFamily;

   ci := TDemoCollectionItem(Self.Add);
   ci.ID := 3;
   ci.LastName := 'Doe';
   ci.FirstName := 'Jonny';
   ci.Category := dcitOther;
end;

{ TDemoStringList }

procedure TDemoStringList.CreateTestData;
begin
   Self.Add('Hallo Welt');
   Self.Add('Hello World');
   Self.Add('Slava Ukraini');
   Self.Add('Women, Life, Freedom');
end;

{ TDemoObjectListItem }

function TDemoObjectListItem.GetImageIndex: integer;
begin
   Result := 1;
end;

procedure TDemoObjectListItem.Execute(Sender: TObject);
begin
   MessageDlg('TDemoObjectListItem clicked', Self.Title, mtInformation, [mbOK], 0);
end;

constructor TDemoObjectListItem.Create;
begin
   Self.FOnExecute := Execute;
end;


{ TDemoObjectList }

procedure TDemoObjectList.CreateTestData;
var
   dd: TDemoObjectListItem;
begin
   dd := TDemoObjectListItem.Create;
   dd.Title := 'Once Upon A Time...';
   dd.Description := 'A fairytale from the past.';
   dd.ReleaseDate := EncodeDate(1964, 12, 12);
   dd.Media := 'Book';
   dd.MinimumAge := dolin15Years;
   Self.Add(dd);
   dd := TDemoObjectListItem.Create;
   dd.Title := 'The Science of Space';
   dd.Description := 'A popular tale about black holes.';
   dd.ReleaseDate := EncodeDate(2001, 1, 31);
   dd.Media := 'Non Fiction';
   dd.MinimumAge := dolin15Years;
   Self.Add(dd);
   dd := TDemoObjectListItem.Create;
   dd.Title := 'Night of bugfixing';
   dd.Description := 'A horror tale.';
   dd.ReleaseDate := EncodeDate(2022, 12, 27);
   dd.Media := 'Movie';
   dd.MinimumAge := dolin21Years;
   Self.Add(dd);
end;

{ TDemoSimpleList }

procedure TDemoSimpleList.Notify(const AValue: TDemoObjectListItem; ACollectionNotification: TCollectionNotification);
begin
   inherited Notify(AValue, ACollectionNotification);

   if (ACollectionNotification = cnRemoved) then
      AValue.Free;
end;

destructor TDemoSimpleList.Destroy;
begin
   inherited Destroy;
end;

function TDemoSimpleList.GetEnumerator: TDemoSimpleListEnumerator;
begin
   Result := TDemoSimpleListEnumerator.Create(Self);
end;

procedure TDemoSimpleList.CreateTestData;
var
   dd: TDemoObjectListItem;
begin
   dd := TDemoObjectListItem.Create;
   dd.Title := 'Once Upon A Time...';
   dd.Description := 'A fairytale from the past.';
   dd.ReleaseDate := EncodeDate(1964, 12, 12);
   dd.Media := 'Book';
   Self.Add(dd);
   dd := TDemoObjectListItem.Create;
   dd.Title := 'The Science of Space';
   dd.Description := 'A popular tale about black holes.';
   dd.ReleaseDate := EncodeDate(2001, 1, 31);
   dd.Media := 'Book';
   Self.Add(dd);
   dd := TDemoObjectListItem.Create;
   dd.Title := 'Night of bugfixing';
   dd.Description := 'A horror tale.';
   dd.ReleaseDate := EncodeDate(2022, 12, 27);
   dd.Media := 'Movie';
   Self.Add(dd);
end;

{ TDemoSimpleListEnumerator }

function TDemoSimpleListEnumerator.DoGetCurrent: TDemoObjectListItem;
begin
   Result := FList[FPosition];
end;

function TDemoSimpleListEnumerator.DoMoveNext: boolean;
begin
   Inc(FPosition);
   Result := FPosition < FList.Count;
end;

constructor TDemoSimpleListEnumerator.Create(AList: TDemoSimpleList);
begin
   FList := AList;
   FPosition := -1;
end;

end.
