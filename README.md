
# Pascal Object Visualizing Mapping

Separating user interface and data is a key concept in software development, yet 
RAD (Rapid Access Development) environments like Delphi or Lazarus make it easy
to discard that concept. Way too often I've seen controls like TListView used
as a data storage, for example.

OVM is intended to make it easier to separate the data by providing an automated
way of displaying it. You can let the code in this repository take over the 
display of your data.

All you have to do to use it is define published properties (you can use
attributes to finetune their display), which will be filled into TListView
columns created on the fly.

## Supported Controls

### Features supported on all controls
* Custom field formatting

### TListView
* Support for Windows Groups
* Support for ImageIndex
* Support for OnExecute event property (double-click)
* Keeps selection on refresh

### TComboBox
* Keeps selection on refresh

### TComboBoxEx
* Support for ImageIndex
* Support for OnExecute event property (double-click)
* Keeps selection on refresh

### TTreeView
* Support for ImageIndex
* Support for OnExecute event property (double-click)
* Keeps selection on refresh
* Hierarchy through IDs or parent objects

## TVirtualStringTree
* Support for ImageIndex
* Support for OnExecute event property (double-click)
* Keeps selection on refresh
* Hierarchy through IDs or parent objects

## Examples

Heavy use of this features is made in [aCropAlyzer](https://gitlab.com/spybot/acropalyzer/-/blob/main/source/aCropAlyzer.FileScan.pas):

```
type

   { TACropAlyzerFileScan }

   TACropAlyzerFileScan = class
   private
      // ...
   public
      // ...
   published
      [APropertyListViewGroup('Summary'), APropertyListViewBooleanKeyItemIndexes(0, 1), APropertyListViewBooleanStringValues('This file is Safe to Share', 'Do NOT share this file, strip sensible information first!')]
      property Summary: boolean read FSummary;
      [APropertyListViewGroup('File Properties')]
      property Size: int64 read FFileSize;
      [APropertyListViewGroup('File Properties')]
      property Date: TDateTime read FFileDate;
      [APropertyListViewGroup('aCropalypsis'), APropertyListViewKeyName('Affected File Type'), APropertyListViewBooleanYesNo()]
      property IsPNGFile: boolean read FIsPNGFile;
      [APropertyListViewGroup('aCropalypsis'), APropertyListViewKeyName('Includes Hidden Data'), APropertyListViewBooleanKeyItemIndexes(1, 0), APropertyListViewBooleanYesNo()]
      property IsAcropalypse: boolean read FIsAcropalypse;
      [APropertyListViewGroup('aCropalypsis'), APropertyListViewKeyName('Visible File Size'), APropertyListViewSkipIfZero()]
      property AcropalypseCroppedSize: int64 read FAcropalypseCroppedSize;
      [APropertyListViewGroup('EXIF'), APropertyListViewKeyName('Supports EXIF Information'), APropertyListViewBooleanYesNo()]
      property IsEXIFFile: boolean read FIsEXIFFile;
      [APropertyListViewGroup('EXIF'), APropertyListViewKeyName('Includes EXIF Information'), APropertyListViewBooleanYesNo()]
      property HasEXIFData: boolean read FHasEXIFData;
      [APropertyListViewGroup('EXIF'), APropertyListViewKeyName('Artist'), APropertyListViewSkipIfEmpty()]
      property EXIFArtist: string read FEXIFArtist;
      [APropertyListViewGroup('EXIF'), APropertyListViewKeyName('Camera Make'), APropertyListViewSkipIfEmpty()]
      property EXIFCameraMake: string read FEXIFCameraMake;
      [APropertyListViewGroup('EXIF'), APropertyListViewKeyName('Camera Model'), APropertyListViewSkipIfEmpty()]
      property EXIFCameraModel: string read FEXIFCameraModel;
      [APropertyListViewGroup('EXIF'), APropertyListViewKeyName('Comment'), APropertyListViewSkipIfEmpty()]
      property EXIFComment: string read FEXIFComment;
      [APropertyListViewGroup('EXIF'), APropertyListViewKeyName('Copyright'), APropertyListViewSkipIfEmpty()]
      property EXIFCopyright: string read FEXIFCopyright;
      [APropertyListViewGroup('EXIF'), APropertyListViewKeyName('Includes Location Data'), APropertyListViewBooleanKeyItemIndexes(1, 0), APropertyListViewBooleanYesNo()]
      property EXIFIncludesLocation: boolean read FEXIFIncludesLocation;
   end;

```